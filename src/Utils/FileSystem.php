<?php
/**
 * Namespace for Utils class
 */
namespace Utils;

use Utils\Printer;

/**
 * Class for work with filesystem - save/delete - file/folder
 */
class FileSystem
{
	// Project path - root path
	public static $projectDir;

	public static function setProjectDir($projectDir)
	{
		if (!isset(self::$projectDir))
		{
			self::$projectDir = $projectDir;
		}
		return self::$projectDir;
	}

	public static function getFile($path)
	{
		return fopen(self::$projectDir.$path,"rw");
	}

	public static function getProjectDir()
	{
		return self::$projectDir;
	}

	public static function getFileContents($path)
	{
		return (self::exist($path)) ? file_get_contents(self::$projectDir.$path,"rw") : false;
	}

	public static function save($path, $content)
	{
		// check if is string path or not
		$split_path = explode("/",$path);

		if (count($split_path)>0) {
			// get file name
			$file_name = array_pop($split_path);

			//only directories path
			$fpath = str_replace($file_name,"",$path);

			//check if exist folder/path
			if (!file_exists(str_replace($file_name,"",$path))) {
				//create directories path
				self::createDir(array_reverse($split_path));
			}

			//save file - if file exist => rewrite file
			Printer::cli("File SAVE TO: ".self::$projectDir.$fpath.$file_name,'sucess');
			file_put_contents(self::$projectDir.$fpath.$file_name,$content);
			return;
		}

		//save file - if file exist => rewrite file
		Printer::cli("REWRITE file: ".self::$projectDir.$fpath.$file_name,'warning');
		file_put_contents($fpath.$file_name, $content);
		chmod($fpath.$file_name, 0777);
	}

	public static function olderThen($time, $path)
	{
		if (!self::exist($path)) return false;

		if (filemtime(self::$projectDir.$path) > $time)
			return true;
		return false;
	}

	public static function newerThen($time, $path)
	{
		if (!self::exist($path)) return false;

		if (filemtime(self::$projectDir.$path) < $time)
			return true;
		return false;
	}

	// Control if file path or folder exist
	public static function exist($path)
	{
		if (file_exists(self::$projectDir.$path)) {
			return true;
		}
		return false;
	}

	// create recursive folder path
	private static function createDir($list, $fpath=false)
	{
		// get first name for dir
		$dir_name = array_pop($list);

		//create path for create new directory
		if (!$fpath) {
			$fpath = self::$projectDir.$dir_name;
		}else{
			if ($fpath===true)$fpath="/";
			$fpath .= $dir_name;
		}

		//create directory + check if folder already exist
		if (!file_exists($fpath)) {
			mkdir($fpath);
		}

		// check if list have some directory for create
		if (count($list)>0) {
			// call again function for create directory
			self::createDir($list,$fpath."/");
		}

		$fpath.="/";
		return $fpath;
	}
}
