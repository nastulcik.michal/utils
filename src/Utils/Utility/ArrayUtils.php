<?php
/**
 * Namespace for Utils class
 */
namespace Utils\Utility;

/**
* Helper class for better work with array
*/
class ArrayUtils
{
	/**
	 * Return only match sent key in input array
	 * @param  array  $input Input array
	 * @param  array  $keys  Array with allowed keys
	 * @return array         Return filter array by $keys
	 */
	public static function filterKeys(array $input, array $keys)
	{
		return array_intersect_key($input, array_flip($keys));
	}

	/**
	 * Filter key by sent prefix
	 * @param  string $prefix Prefix for filter
	 * @param  array  $input  Input array
	 * @return array          Return array with only keys with sent prefix
	 */
	public static function filterKeysPrefix($prefix, array $input)
	{
		$output = array();
		foreach ($input as $key => $value)
		{
			if (mb_strpos($key, $prefix) === 0)
				$output[$key] = $value;
		}
		return $output;
	}

	/**
	 * Map pairs key-value
	 * @param  array  $rows     ArrayArray - 2-dimensional array
	 * @param  string $keyKey   Map key
	 * @param  string $valueKey Map value
	 * @return array            Return array like key-value
	 */
	public static function mapPairs(array $rows, $keyKey, $valueKey)
	{
		$pairs = array();
		
		foreach ($rows as $row)
		{
			$key = $row[$keyKey];
			
			// Control collision key
			if (isset($pairs[$key]))
			{
				$i = 1;
				while (isset($pairs[$key . ' (' . $i . ')']))
				{
					$i++;
				}
				$key .= ' (' . $i . ')';
			}
			$pairs[$key] = $row[$valueKey];
		}
		return $pairs;
	}

	/**
	 * Reindex 2-dimensional array by index value in array
	 * @param  array  $rows     ArrayArray - 2-dimensional array
	 * @param  string $index_name   Index key name for reindex array
	 * @return array            Reindex array by value 
	 */
	public static function reindexArrByVal(array $rows, string $index_name): array
	{
		$arr = [];
		
		foreach ($rows as $row)
		{
			$arr[$row[$index_name]] = $row;
		}
		return $arr;
	}

	/**
	 * From 2-dimensional array create only one dimensional array
	 * @param  array  $rows      2-dimensional array
	 * @param  string $singleKey Key 
	 * @return array             Return array like - index => value
	 */
	public static function mapSingles(array $rows, $singleKey)
	{
		$singles = array();
		foreach ($rows as $row)
		{
			$singles[] = $row[$singleKey];
		}
		return $singles;
	}

	/**
	 * Add prefix to keys in array
	 */
	public static function addPrefix($prefix, array $input)
	{
		$output = array();
		foreach ($input as $key => $value)
		{
			$key = $prefix . $key;
			if (is_array($value))
				$value = self::addPrefix($prefix, $value);
			$output[$key] = $value;
		}
		return $output;
	}

	/**
	 * Remove prefix to keys in array
	 */
	public static function removePrefix($prefix, array $input)
	{
		$output = array();
		foreach ($input as $key => $value)
		{
			if (strpos($key, $prefix) === 0)
				$key = substr($key, mb_strlen($prefix));
			if (is_array($value))
				$value = self::removePrefix($prefix, $value);
			$output[$key] = $value;
		}
		return $output;
	}

	/**
	 * Array keys by notation camelToSnake 
	 * @param array $inputArray 
	 * @return  array
	 */
	public static function camelToSnake(array $inputArray) : array
	{
		$outputArray = array();
		foreach ($inputArray as $key => $value)
		{
			$key = StringUtils::camelToSnake($key);
			if (is_array($value))
				$value = self::camelToSnake($value);
			$outputArray[$key] = $value;
		}
		return $outputArray;
	}

	/**
	 * Array keys by notation snakeToCamel 
	 * @param array $inputArray 
	 * @return  array
	 */
	public static function snakeToCamel(array $inputArray): array
	{
		$outputArray = array();
		foreach ($inputArray as $key => $value)
		{
			$key = StringUtils::snakeToCamel($key);
			if (is_array($value))
				$value = self::snakeToCamel($value);
			$outputArray[$key] = $value;
		}
		return $outputArray;
	}

	/**
	 * Unset array values by keys
	 *
	 * @param array $arr
	 * @param array $keys
	 * @return array Array without unset keys
	 */
	public static function unsetElementsByKeys(array $arr, array $keys) : array
	{
		foreach ($keys as $key) {
			if (isset($arr[$key])) {
				unset($arr[$key]);
			}
		}
		return $arr;
	}
}
