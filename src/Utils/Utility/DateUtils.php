<?php

/**
 * Namespace for Utils class
 */

namespace Utils\Utility;

use InvalidArgumentException;

/**
 * Helper class for better work with date
 * in czech date-time format
 */
class DateUtils
{

	const DATETIME_FORMAT = 'Y-m-d HH:ii';
	const DATE_FORMAT = 'Y-m-d';
	const TIME_FORMAT = 'H:i:s';

	// Database formats
	const DB_DATETIME_FORMAT = 'Y-m-d H:i:s';
	const DB_DATE_FORMAT = 'Y-m-d';
	const DB_TIME_FORMAT = 'H:i:s';

	/**
	 * List all czech months
	 * @var array
	 */
	private static $months = array('ledna', 'února', 'března', 'dubna', 'května', 'června', 'července', 'srpna', 'září', 'října', 'listopadu', 'prosince');

	/**
	 * List all error types
	 * @var array
	 */
	private static $errorMessages = array(
		self::DATE_FORMAT => 'Neplatné datum, zadejte ho prosím ve tvaru dd.mm.rrrr',
		self::TIME_FORMAT => 'Neplatný čas, zadejte ho prosím ve tvaru hh:mm, můžete dodat i vteřiny',
		self::DATETIME_FORMAT => 'Neplatné datum nebo čas, zadejte prosím hodnotu ve tvaru dd.mm.rrrr hh:mm, případně vteřiny',
	);
	/**
	 * Type formats
	 * @var array
	 */
	private static $formatDictionary = array(
		self::DATE_FORMAT => self::DB_DATE_FORMAT,
		self::DATETIME_FORMAT => self::DB_DATETIME_FORMAT,
		self::TIME_FORMAT => self::DB_TIME_FORMAT,
	);

	/**
	 * Valid date catch ErrorException
	 * @param  string $date   input date
	 * @param  string $format type format for date
	 * @return boolean
	 */
	public static function validDate($date, $format = self::DATETIME_FORMAT)
	{
		try {
			self::parseDateTime($date, $format);
			return true;
		} catch (InvalidArgumentException $e) {
		}
		return false;
	}

	/**
	 * Return actual time in czech format
	 * @return string czech actual date time
	 */
	public static function dbNow()
	{
		$dateTime = new \DateTime();
		return $dateTime->format(self::DB_DATETIME_FORMAT);
	}

	/**
	 * Create new instance DateTime
	 * @param  string $date Input date
	 * @return object       Instance DateTime
	 */
	public static function getDateTime($date)
	{
		if (ctype_digit($date))
			$date = '@' . $date;
		return new \DateTime($date);
	}

	/**
	 * Format date to czech format
	 * @param  string $date Input date
	 * @return string       Czech format
	 */
	public static function formatDate($date)
	{
		$dateTime = self::getDateTime($date);
		return $dateTime->format('d.m.Y');
	}

	/**
	 * Format date + time [ hours:minutes:seconds ] to czech format
	 * @param  string $date Input date
	 * @return string       Czech format with H:m:s
	 */
	public static function formatDateTime($date)
	{
		$dateTime = self::getDateTime($date);
		return $dateTime->format('d.m.Y H:i:s');
	}

	/**
	 * Parse date on pretty human readable format
	 * @param  object $dateTime Instance DateTime
	 * @return string           Return pretty date
	 */
	private static function getPrettyDate($dateTime)
	{
		$now = new \DateTime();

		if ($dateTime->format('Y') != $now->format('Y'))
			return $dateTime->format('j.n.Y');

		$dayMonth = $dateTime->format('d-m');

		if ($dayMonth == $now->format('d-m'))
			return "Dnes";

		$now->modify('-1 DAY');
		if ($dayMonth == $now->format('d-m'))
			return "Včera";

		$now->modify('+2 DAYS');
		if ($dayMonth == $now->format('d-m'))
			return "Zítra";

		return $dateTime->format('j.') . self::$months[$dateTime->format('n') - 1];
	}

	/**
	 * Return pretty human readable date
	 * @param  string $date
	 * @return string
	 */
	public static function prettyDate($date)
	{
		return self::getPrettyDate(self::getDateTime($date));
	}

	/**
	 * [prettyDateTime description]
	 * @param  [type] $date [description]
	 * @return [type]       [description]
	 */
	public static function prettyDateTime($date)
	{
		$dateTime = self::getDateTime($date);
		return self::getPrettyDate($dateTime) . $dateTime->format(' H:i:s');
	}

	/**
	 * Parse date from frotend format to MYSQL format
	 * @param  string $date   Input date
	 * @param  string $format Type input date
	 * @return string         MYSQL format
	 */
	public static function parseDateTime($date, $format = self::DATETIME_FORMAT)
	{
		
		if ($format == "Y-m-d HH:ii") {
			$time = strtotime($date);
			return $dateTime = date("Y-m-d H:i:s", $time);
			// return $dateTime->format(self::$formatDictionary[$format]);
		}
		if ($format == 'Y-m-d') {
			$dateTime = \DateTime::createFromFormat($format, $date);
			return $dateTime->format(self::$formatDictionary[$format]);
		}

		if (mb_substr_count($date, ':') == 1)
			$date .= ':00';

		// Delete spaces before and after separators
		$a = array('/([\.\:\/])\s+/', '/\s+([\.\:\/])/', '/\s{2,}/');
		$b = array('\1', '\1', ' ');
		$date = trim(preg_replace($a, $b, $date));

		// Delete zeros before numbers
		$a = array('/^0(\d+)/', '/([\.\/])0(\d+)/');
		$b = array('\1', '\1\2');
		$date = preg_replace($a, $b, $date);

		// Create instance DateTime , which control if set date exist
		$dateTime = \DateTime::createFromFormat($format, $date);
		$errors = \DateTime::getLastErrors();

		// Catch Exception
		if ($errors['warning_count'] + $errors['error_count'] > 0) {
			if (in_array($format, self::$errorMessages))
				throw new InvalidArgumentException(self::$errorMessages[$format]);
			else
				throw new InvalidArgumentException('Neplatná hodnota');
		}

		// Return data in MYSQL format
		return $dateTime->format(self::$formatDictionary[$format]);
	}
}
