<?php

namespace Utils\Forms;

use Exception;

/**
 * Třída reprezentuje výjimku formuláře
 */
class FormException extends Exception
{

}