<?php

namespace Utils\Forms;

use Utils\Forms\Controls\CheckBox;
use Utils\Forms\Controls\CheckList;
use Utils\Forms\Controls\DateTimePicker;
use Utils\Forms\Controls\FileBox;
use Utils\Forms\Controls\InputBox;
use Utils\Forms\Controls\ListBox;
use Utils\Forms\Controls\RadioGroup;
use Utils\Forms\Controls\TextArea;
use Utils\HtmlBuilder;
use Utils\UserException;
use Utils\Utility\DateUtils;

class Form
{
	/**
	 * HTTP metoda POST
	 */
	const METHOD_POST = 'POST';
	/**
	 * HTTP metoda GET
	 */
	const METHOD_GET = 'GET';
	/**
	 * @var array Kolekce kontrolek formuláře
	 */
	private $controls = array();
	/**
	 * @var bool Zda byl formulář odeslaný
	 */
	private $isPostBack = false;
	/**
	 * @var string Název formuláře
	 */
	public $formName;
	/**
	 * @var array Kolekce tlačítek
	 */
	private $buttons = array();
	/**
	 * @var array Kolekce vyrenderovaných kontrolek
	 */
	private $rendered = array();
	/**
	 * @var bool Zda se mají generovat klientské validace
	 */
	public $validateClient = true;
	/**
	 * @var bool Zda se má spouštět serverová validace
	 */
	public $validateServer = true;
	/**
	 * @var bool Zda jsou hodnoty ve formuláři validní
	 */
	private $valid = false;
	/**
	 * @var array Mapa klíčů formuláře na jeho kontrolky
	 */
	private $keymap = null;
	/**
	 * @var bool Zda je ve formuláři kontrolka pro nahrávání souborů, používá se při generování enctype
	 */
	private $hasFile;
	/**
	 * @var bool Zda se má formulář renderovat jako řádek
	 */
	private $inline;
	/**
	 * @var string HTTP metoda formuláře
	 */
	private $method;
	/**
	 * @var boolean Pokud je zapnuto automaticky se přidají bootsrap třídy pro styly
	 */
	public $bootstrap = true;
	/**
	 * @var boolean Zda se má u kontrolek vytvařet label
	 */
	private $label = true;
	/**
	 * @var string Form action
	 */
	private $form_action = '';


	/**
	 * Inicializuje formulář
	 * @param string $formName Název formuláře
	 * @param string $method HTTP metoda, kterou se má formulář odeslat
	 * @param bool $inline Zda se má formulář renderovat do řádku
	 */
	public function __construct($formName, $method = self::METHOD_POST, $inline = false)
	{
		$this->formName = $formName;
		$this->method = $method;
		$this->inline = $inline;
		$formNameBox = $this->addHiddenBox('form-name', $this->formName);
		$this->isPostBack = $formNameBox->sentDataKeyExists('form-name') && $formNameBox->getSentData('form-name') == $this->formName;
	}

	/**
	 * Vrátí hodnotu tlačítka, kterým se odeslal formulář
	 * @return null Hodnota tlačítka nebo NULL
	 */
	public function getSentButton()
	{
		foreach ($this->buttons as $button)
		{
			if ($button->sentDataKeyExists($button->name))
				return $button->name;
		}
		return null;
	}


	/**
	 * Přída bootstrap třídu pokud je bootstrap povolen
	 * @param object $element Vráti
	 */
	public function addBootstrapClass($element)
	{
		if(!$this->bootstrap) return $element;

		/* All input types */
		/*
		<input type="button">
		<input type="checkbox">
		<input type="color">
		<input type="date">
		<input type="datetime-local">
		<input type="email">
		<input type="file">
		<input type="hidden">
		<input type="image">
		<input type="month">
		<input type="number">
		<input type="password">
		<input type="radio">
		<input type="range">
		<input type="reset">
		<input type="search">
		<input type="submit">
		<input type="tel">
		<input type="text">
		<input type="time">
		<input type="url">
		<input type="week">
		custom-select - select
		 */
		
		$b4Classes = [
			'button' => "btn",
			'checkbox' => "form-check-input",
			'color' => "form-control",
			'date' => "form-control",
			'file' => "custom-file-input",
			'range' => 'form-control-range',
			'submit' => 'btn'
		];

		$b4Class = "form-control";

		if (isset($element->type) && isset($b4Classes[$element->type])) {
			$b4Class = $b4Classes[$element->type];
		}

		if(isset($element->htmlParams['class'])){
			$element->htmlParams['class'] = $b4Class . " " . $element->htmlParams['class'];
			return $element;
		}
		$element->htmlParams['class'] = $b4Class;
		return $element;
	}
	
	/**
	 * Přidá kontrolku do formuláře
	 * @param FormControl $control Kontrolka
	 * @param bool $required Zda je kontrolka povinná
	 */
	private function addControl($control, $required = false)
	{
		if ($required)
			$control->addRequiredRule();
		$control->setMethod($this->method);
		$this->controls[$control->htmlParams['name']] = $control;
		return $control;
	}

	/**
	 * Přidá do formuláře InputBox určitého typu
	 * @param string $name Název inputu
	 * @param string $type Typ
	 * @param string $pattern Regulární výraz pro kontrolu typu na serveru
	 * @param string $label Popisek
	 * @param bool $required Zda je pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return InputBox InputBox
	 */
	private function addInputBox($name, $type, $pattern, $label, $required = false, $htmlParams = array())
	{
		$textBox = new InputBox($name, $type, $label, $htmlParams);
		if ($pattern)
			$textBox->addPatternRule($pattern, false, true);
		return $this->addControl($textBox, $required);		
	}

	/**
	 * Přidá do formuláře textové pole
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return InputBox Textové pole
	 */
	public function addTextBox($name, $label, $required = false, $htmlParams = array())
	{
		return $this->addInputBox($name, 'text', null, $label, $required, $htmlParams);
	}

	/**
	 * Přidá do formuláře skryté pole
	 * @param string $name Název pole
	 * @param string $text Text v poli
	 * @param bool $required Zda je pole povinné
	 * @return InputBox Skryté pole
	 */
	public function addHiddenBox($name, $text = '', $required = false)
	{
		return $this->addInputBox($name, 'hidden', null, '', $required)
					->setText($text);
	}

	/**
	 * Přidá recaptchu
	 * @param float $securityValue Maximální bezbečnostní hodnota ( 1 až 0 ) - 1 je nejbezpečnější
	 */
	public function addRecaptchaBox($securityKey, $siteKey, $securityValue)
	{
		return $this->addInputBox("recaptcha_response", 'hidden', null, 'Recaptcha', true, 
			['id' => "recaptchaResponse", "securityKey" => $securityKey, "site_key" => $siteKey,]
		)->addRecaptchaRule($securityKey, $siteKey, $securityValue);
	}

	/**
	 * Insert javascript to html for function Google recaptcha V3
	 * @return string Html or empty string
	 */
	public function renderRecaptchaJs()
	{
		// Control if recaptcha using in form
		if (!isset($this->controls["recaptcha_response"])) {
			return "";
		}
		$recaptcha = $this->controls["recaptcha_response"]->htmlParams;

		$html = '<script src="https://www.google.com/recaptcha/api.js?render=' . $recaptcha["site_key"] .'"></script>
		<script>
		    grecaptcha.ready(function() {
		        grecaptcha.execute("' . $recaptcha["site_key"] . '", {action: "contact"}).then(function(token) {
		           var recaptchaResponse = document.getElementById("recaptcha_response");
		           recaptchaResponse.value = token;
		        });
		    });
		</script>';

		return $html;
	}

	/**
	 * Přidá do formuláře pole pro zadání emailu
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return InputBox InputBox
	 */
	public function addEmailBox($name, $label, $required = false, $htmlParams = array())
	{
		return $this->addInputBox($name, 'email', FormControl::PATTERN_EMAIL, $label, $required, $htmlParams);
	}

	/**
	 * Přidá do formuláře pole na URL adresu
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return InputBox InputBox
	 */
	public function addUrlBox($name, $label, $required = false, $htmlParams = array())
	{
		return $this->addInputBox($name, 'url', FormControl::PATTERN_URL, $label, $required, $htmlParams);
	}

	/**
	 * Přidá do formuláře pole pro zadání celého čísla
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return InputBox InputBox
	 */
	public function addNumberBox($name, $label, $required = false, $htmlParams = array())
	{
		return $this->addInputBox($name, 'number', FormControl::PATTERN_INTEGER, $label, $required, $htmlParams);
	}

	/**
	 * Přidá do formuláře pole pro zadání float čísla
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return InputBox InputBox
	 */
	public function addFloatNumberBox($name, $label, $required = false, $htmlParams = array())
	{
		return $this->addInputBox($name, 'float', FormControl::PATTERN_FLOAT, $label, $required, $htmlParams);
	}

	/**
	 * Přidá do formuláře pole pro heslo
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return InputBox InputBox
	 */
	public function addPasswordBox($name, $label, $required = false, $htmlParams = array())
	{
		return $this->addInputBox($name, 'password', '', $label, $required, $htmlParams)
					->addPasswordRule();
	}

	/**
	 * Přidá do formuláře tlačítko
	 * @param string $name Název pole
	 * @param string $text Text na tlačítku
	 * @param array $htmlParams HTML parametry
	 * @return InputBox InputBox
	 */
	public function addButton($name, $text, $htmlParams = array())
	{
		$button = $this->addInputBox($name, 'submit', '', '', false, $htmlParams)
					   ->setText($text);
		$this->buttons[$name] = $button;
		return $button;
	}

	/**
	 * Přidá do formuláře zaškrtávací pole
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $checked Zda má být ve výchozím stavu zaškrtnutý
	 * @param array $htmlParams HTML parametry
	 * @return CheckBox CheckBox
	 */
	public function addCheckBox($name, $label, $checked = false, $htmlParams = array())
	{
		return $this->addControl(new CheckBox($name, $label, $htmlParams))
			->setChecked($checked);
	}

	/**
	 * Přidá do formuláře pole pro víceřádkový text
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je vyplnění pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return TextArea TextArea
	 */
	public function addTextArea($name, $label, $required = false, $htmlParams = array())
	{
		return $this->addControl(new TextArea($name, $label, $htmlParams), $required);
	}

	/**
	 * Přidá do formuláře pole pro výběr data a času
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je vyplnění pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return DateTimePicker DateTimePicker
	 */
	public function addDateTimePicker($name, $label, $required = false, $htmlParams = array())
	{
		if (!isset($htmlParams['placeholder'])) {
			$htmlParams['placeholder'] = 'dd.mm.yyyy hh:mm';
		}
		return $this->addControl(new DateTimePicker($name, DateUtils::DATETIME_FORMAT, $label, $htmlParams), $required)
			->addDateTimeRule();
	}

	/**
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je vyplnění pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return DateTimePicker DateTimePicker
	 */
	public function addDatePicker($name, $label, $required = false, $htmlParams = array())
	{
		// TODO: fix it better - HOT FIX FOR ENGLISH DATE
		// return $this->addControl(new TextArea($name, $label, $htmlParams), $required);
		if (!isset($htmlParams['placeholder'])) {
			$htmlParams['placeholder'] = 'dd.mm.yyyy';
		}
		return $this->addControl(new DateTimePicker($name, DateUtils::DATE_FORMAT, $label, $htmlParams), $required);
	}

	/**
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je vyplnění pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return DateTimePicker DateTimePicker
	 */
	public function addTimePicker($name, $label, $required = false, $htmlParams = array())
	{
		if (!isset($htmlParams['placeholder'])) {
			$htmlParams['placeholder'] = 'hh:mm:ss';
		}
		return $this->addControl(new DateTimePicker($name, DateUtils::TIME_FORMAT, $label, $htmlParams), $required)
			->addTimeRule();
	}

	/**
	 * Přidá do formuláře pole pro výběr souboru
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param string $accept Maska pro typy souborů
	 * @param bool $multiple Zda lze vybrat více souborů
	 * @param array $htmlParams HTML parametry
	 * @return FileBox FileBox
	 */
	public function addFileBox($name, $label, $required = false, $accept = null, $multiple = false, $htmlParams = array())
	{
		if ($accept)
			$htmlParams['accept'] = $accept;
		$this->hasFile = true;
		return $this->addControl(new FileBox($name, $required, $multiple, $label, $htmlParams));
	}

	/**
	 * Přidá do formuláře rozbalovací nabídku
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je pole povinné
	 * @param array $htmlParams HTML parametry
	 * @return ListBox ListBox
	 */
	public function addComboBox($name, $label, $required = false, $htmlParams = array(), $firstOptionName = false)
	{
		$comboBox = new ListBox($name, $required, false, $label, $htmlParams, $firstOptionName);
		$this->addControl($comboBox, $required);
		return $comboBox;
	}

	/**
	 * Přidá do formuláře výběr ze seznamu hodnot
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $required Zda je pole povinné
	 * @param bool $multiple Zda jde vybrat více hodnot
	 * @param array $htmlParams HTML parametry
	 * @return ListBox ListBox
	 */
	public function addListBox($name, $label, $required = false, $multiple = false, $htmlParams = array())
	{
		if (!$multiple)
			$htmlParams['size'] = 4;
		$comboBox = new ListBox($name, $required, $multiple, $label, $htmlParams);
		$this->addControl($comboBox, $required);
		return $comboBox;
	}

	/**
	 * Přidá do formuláře pole s několika souvisejícími CheckBoxy
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $horizontal Zda chceme položky řadit vedle sebe nebo pod sebe
	 * @param array $htmlParams HTML parametry
	 * @return CheckList CheckList
	 */
	public function addCheckList($name, $label, $horizontal = false, $htmlParams = array())
	{
		$checkList = new CheckList($name, $label, $htmlParams);
		$checkList->setHorizontal($horizontal);
		$this->addControl($checkList);
		return $checkList;
	}

	/**
	 * Přidá do formuláře skupinu RadioButtonů
	 * @param string $name Název pole
	 * @param string $label Popisek
	 * @param bool $horizontal Zda chceme položky řadit vedle sebe nebo pod sebe
	 * @param array $htmlParams HTML parametry
	 * @return RadioGroup RadioGroup
	 */
	public function addRadioGroup($name, $label, $horizontal = false, $htmlParams = array())
	{
		$radioGroup = new RadioGroup($name, $label, $htmlParams);
		$radioGroup->setHorizontal($horizontal);
		$this->addControl($radioGroup, true);
		return $radioGroup;
	}

	/**
	 * Vrátí HTML kód celého formuláře
	 * @return string HTML kód celého formuláře
	 */
	public function render()
	{
		$s = $this->renderStartForm();
		$s .= $this->renderControls();
		$s .= $this->renderButtons();
		$s .= $this->renderEndForm();
		return $s;
	}

	public function setFormAction(string $action) : void
	{
		$this->form_action = $action;
	}

	/**
	 * Vrátí HTML kód začátku formuláře
	 * @return string HTML kód začátku formuláře
	 */
	public function renderStartForm()
	{
		$params = array(
			'method' => $this->method,
			'id' => $this->formName,
			'class' => 'fancyform',
			'action' => $this->form_action,
		);
		if ($this->inline)
			$params['class'] .= ' inline-form';
		if ($this->hasFile)
			$params['enctype'] = 'multipart/form-data';
		$builder = new HtmlBuilder();
		$builder->startElement('form', $params);
		foreach ($this->controls as $control)
		{
			if ($control instanceof InputBox && $control->type == 'hidden')
			{
				$builder->addValue($control->render($this->validateClient, $this->isPostBack()), true);
				$this->rendered[$control->name] = $control;
			}
		}
		return $builder->render();
	}

	/**
	 * Vrátí HTML kód konce formuláře
	 * @return string HTML kód
	 */
	public function renderEndForm()
	{
		$recaptchaJs = $this->renderRecaptchaJs();
		$builder = new HtmlBuilder();
		$builder->endElement('form');
		$renderEndForm = $builder->render();
		return $recaptchaJs . $renderEndForm;
	}

	/**
	 * @param $name
	 * @return mixed
	 */
	private function renderControl($name)
	{
		$this->rendered[$name] = $this->controls[$name];
		return $this->controls[$name]->render($this->validateClient, $this->isPostBack);
	}

	/**
	 * Vrátí pole kontrolek podle pole názvů kontrolek
	 * @param array $names Názvy kontrolek
	 * @return array Kontrolky
	 * @throws FormException
	 */
	private function getControlsToRender($names = array())
	{
		// Při nezadaném parametru chceme renderovat vše
		if (!$names)
			$names = array_keys($this->controls);
		else
		{
			// Kontrola neexistujícíh názvů
			$diff = array_diff($names, array_keys($this->controls));
			if ($diff)
				throw new FormException('Ve formuláři neexistuje: ' . implode(', ', $diff));
		}
		// Odečtení tlačítek a již vyrenderovaných
		$controls = array_diff_key($this->controls, $this->buttons, $this->rendered);
		// Výběr jen kontrolek s názvy, které chceme renderovat
		$controls = array_intersect_key($controls, array_flip($names));
		return $controls;
	}

	/**
	 * Vrátí HTML kód určitých kontrolek nebo všech kontrolek pokud nezadáme parametry
	 * @param string $name1 Název první kontrolky
	 * @param string $name2 Název druhé kontrolky
	 * @param string $_ Názvy dalších kontrolek
	 * @return mixed
	 */
	public function renderControls($name1 = null, $name2 = null, $_ = null)
	{
		$controls = $this->getControlsToRender(func_get_args());

		$builder = new HtmlBuilder();
		foreach ($controls as $control)
		{
			$b4class = "form-component";
			
			if(isset($control->type) && $control->type == "checkbox"){
				$b4class = "form-check";
			}

			$builder->startElement('div', array(
				'class' => $b4class,
			));

			if ($this->label) {
				$builder->addValueElement('label', $control->label, array(
					'for' => $control->htmlParams['id'],
				), $control->label === '&nbsp;');
			}

			$control = $this->addBootstrapClass($control);
			$builder->addValue($this->renderControl($control->htmlParams['name']), true);
			$builder->startElement('div', array(
				'class' => 'clear',
			));
			$builder->endElement();
			$builder->endElement();
		}
		return $builder->render();
	}

	/**
	 * Vrátí HTML kód tlačítek
	 * @return string HTML kód
	 */
	public function renderButtons()
	{
		if (!$this->buttons)
			return '';
		$builder = new HtmlBuilder();
		$builder->startElement('div', array(
			'class' => 'form-buttons',
		));
		foreach ($this->buttons as $name => $button)
		{
			$builder->addValue($this->renderControl($name), true);
		}
		$builder->endElement();
		return $builder->render();
	}

	/**
	 * Vrátí data z vybraných formulářových kontrolek. Při volání bez parametrů vrátí všechna data.
	 * @param string $name1 Název první kontrolky
	 * @param string $name2 Název druhé kontrolky
	 * @param string $_ Názvy dalších kontrolek
	 * @return array Data z kontrolek
	 */
	public function getData($name1 = null, $name2 = null, $_ = array())
	{
		if (!$this->valid)
			$this->checkValidity();
		$keys = func_get_args();
		if (!$keys)
			$keys = array_keys($this->controls);
		$controls = array_intersect_key($this->controls, array_flip($keys));
		$controls = array_diff_key($controls, $this->buttons);
		$controls = array_diff_key($controls, array_flip(array('form-name')));
		$data = array();
		foreach ($controls as $control)
		{
			$controlData = $control->getData();
			foreach ($controlData as $key => $value)
			{
				$data[$key] = $value;
			}
		}

		// Smazaní Reacaptchy = recaptcha_response 
		// Není potřeba zasílat jelikož validace probíha již ve Třídě FormControl
		if (isset($data['recaptcha_response'])) {
			unset($data['recaptcha_response']);
		}
		return $data;
	}

	/**
	 * Zvaliduje $_POST podle pravidel a vygeneruje chybové hlášky
	 * @throws UserException
	 * @return boolean Zda je formulář validní
	 */
	private function checkValidity()
	{
		if ($this->validateServer)
		{
			$messages = array();
			foreach($this->controls as $control)
			{
				try
				{
					$control->checkValidity();
				}
				catch (UserException $e)
				{
					$messages[] = $e->getMessage();
				}
			}
			if (!empty($messages))
				throw new UserException(implode("\n", $messages));
		}
		$this->valid = true;
	}

	/**
	 * Vrátí mapu klíčů odesílaných formulářem do POST na kontrolky
	 * @return array Mapa klíčů na kontrolky
	 */
	private function getKeymap()
	{
		if ($this->keymap !== null)
			return $this->keymap;
		$keymap = array();
		foreach ($this->controls as $control)
		{
			$keys = $control->getKeys();
			foreach ($keys as $key)
			{
				$keymap[$key] = $control;
			}
		}
		$this->keymap = $keymap;
		return $keymap;
	}

	/**
	 * Nastaví data kontrolkám ve formuláři
	 * @param array $data Data
	 */
	public function setData($data)
	{
		$keymap = $this->getKeymap();

		foreach ($data as $key => $value)
		{
			if (isset($keymap[$key]))
				$keymap[$key]->setData($key, $value);
		}
	}

	/**
	 * Vrátí zda byl formulář odeslán
	 * @return bool Zda byl formulář odeslán
	 */
	public function isPostBack()
	{
		return $this->isPostBack;
	}

	/**
	 * Vypne lable u kontrolek
	 */
	public function disableLabel()
	{
		$this->label = false;
	}

}