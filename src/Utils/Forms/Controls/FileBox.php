<?php

namespace Utils\Forms\Controls;

use Utils\Forms\FormControl;
use Utils\HtmlBuilder;
use Utils\UserException;

/*
 *
 * Kontrolka k nahrávání souborů
 */
class FileBox extends FormControl
{
	/**
	 * @var bool Zda je nahrání souboru povinné
	 */
	private $required;

	/**
	 * Inicializuje instanci
	 * @param string $name Název pole
	 * @param string $required Zda je pole povinné
	 * @param bool $multiple Zda lze vybrat více souborů
	 * @param string $label Popisek pole
	 * @param array $htmlParams HTML parametry
	 */
	public function __construct($name, $required, $multiple, $label = '', $htmlParams = array())
	{
		parent::__construct($name, $label, $htmlParams);
		$this->required = $required;
		if ($required){
			$this->htmlParams['required'] = 'required';
		}
		if ($multiple)
		{
			$this->htmlParams['name'] .= '[]';
			$this->htmlParams['multiple'] = 'multiple';
		}
	}

	/**
	 * Vrátí data z kontrolky
	 * @return array Data
	 */
	public function getData()
	{
		if ($this->required && empty($_FILES[$this->name]['name'][0]))
			throw new UserException('Je nutné vybrat soubor.');
		return !empty($_FILES[$this->name]['name']) ? array($this->name => $_FILES[$this->name]) : array($this->name => '');
	}

	/**
	 * Nastaví data kontrolce, v tomto případě se nenastavuje nic
	 * @param string $key Klíč
	 * @param string $value hodnota
	 */
	public function setData($key, $value)
	{
		// prázdné
	}

	/**
	 * Vrátí HTML kód kontrolky
	 * @param bool $isPostBack Zda byl odeslaný formulář
	 * @return mixed
	 */
	public function renderControl($isPostBack)
	{
		$this->htmlParams['type'] = 'file';
		$builder = new HtmlBuilder();
		$builder->addElement('input', $this->htmlParams);
		return $builder->render();
	}
}