<?php

namespace Utils\Forms\Controls;

use Utils\Forms\FormControl;
use Utils\HtmlBuilder;

/** 
 * Zaškrtávací CheckBox
 */
class CheckBox extends FormControl
{
	/**
	 * @var bool It is checked
	 */
	private $checked;
	/**
	 * @var string Title beside checkbox
	 */
	private $title;

	/**
	 * Inicialization instance
	 * @param string $name Name
	 * @param string $label Title beside checkbox
	 * @param array $htmlParams HTML params
	 */
	public function __construct($name, $label = '', $htmlParams = array())
	{
		$this->title = $label;
		$this->type = "checkbox";
		$htmlParams['type'] = 'checkbox';
		$htmlParams['value'] = 1;
		parent::__construct($name, '&nbsp;', $htmlParams);
	}

	/**
	 * Render CheckBox
	 * @param bool $isPostBack If form was sent
	 * @return string Final HTML
	 */
	public function renderControl($isPostBack)
	{
		if (($isPostBack) && ($this->sentDataKeyExists($this->name) && $this->getSentData($this->name)) || ((!$isPostBack) && ($this->checked)))
			$this->htmlParams['checked'] = 'checked';
		$builder = new HtmlBuilder();
		$builder->addElement('input', $this->htmlParams);
		// Label za checkboxem
		$builder->addValueElement('label', $this->title, array(
			'for' => $this->htmlParams['id'],
		));
		return $builder->render();
	}

	/**
	 * Set if checkbox is checked
	 * @param bool $checked Zda je CheckBox set
	 * @return $this CheckBox CheckBox
	 */
	public function setChecked($checked)
	{
		$this->checked = $checked;
		return $this;
	}

	/**
	 * Return data from CheckBox
	 * @return array Data
	 */
	public function getData()
	{
		return array($this->name => (int)($this->sentDataKeyExists($this->name) && $this->getSentData($this->name)));
	}

	/**
	 * Set data CheckBox
	 * @param string $key Key
	 * @param bool $checked 
	 */
	public function setData($key, $checked)
	{
		$this->checked = (bool)$checked;
	}
}