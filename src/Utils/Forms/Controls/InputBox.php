<?php
namespace Utils\Forms\Controls;

use Utils\Forms\FormControl;
use Utils\HtmlBuilder;

/*
 * Kontrolka pro renderování inputů
 */
class InputBox extends FormControl
{
	/**
	 * @var string Zadaný text
	 */
	private $text;
	/**
	 * @var string Typ inputu
	 */
	public $type;

	/**
	 * Inicializuje instanci
	 * @param string $name Název kontrolky
	 * @param string $type Typ
	 * @param string $label Popisek
	 * @param array $htmlParams HTML parametry
	 */
	public function __construct($name, $type, $label = '', $htmlParams = array())
    {
		$this->type = $type;
        parent::__construct($name, $label, $htmlParams);
    }

	/**
	 * Vrátí HTML kód kontrolky
	 * @param bool $isPostBack Zda byl odeslán formulář
	 * @return string HTML kód
	 */
	public function renderControl($isPostBack)
	{
		$value = ($isPostBack && $this->sentDataKeyExists($this->name) && $this->type != 'password') ? $this->getSentData($this->name) : $this->text;
		$this->htmlParams['value'] = $value;
		$this->htmlParams['type'] = $this->type;
        $builder = new HtmlBuilder();
        $builder->addElement('input', $this->htmlParams);
        return $builder->render();
    }

	/**
	 * Nastaví zadaný text
	 * @param string $text Text
	 * @return InputBox $this Kontrolka pro další použití
	 */
	public function setText($text)
	{
		$this->text = $text;
		return $this;
	}

	/**
	 * Nastaví kontrolce data
	 * @param string $key Klíč, zde se nepoužívá
	 * @param string $text Zadaný text
	 */
	public function setData($key, $text)
	{
		if ($this->type != 'password')
			$this->text = $text;
	}
}
