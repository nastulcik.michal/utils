<?php

namespace Utils\Forms\Controls;

use Utils\Forms\FormControl;
use Utils\Forms\FormException;
use Utils\HtmlBuilder;

/*
 * CheckList je skupina několika CheckBoxů, které se chovají jako by byly samostatné
 */
class CheckList extends FormControl
{
	/**
	 * @var array Vybrané hodnoty
	 */
	private $selectedValues = array();
	/**
	 * @var array Zakázané hodnoty, které nelze měnit
	 */
	private $disabledValues = array();
	/**
	 * @var array Hodnoty k vybrání
	 */
	private $values = array();

	/**
	 * Vrátí data z CheckListu tak, že jsou v nich i nezaškrtnuté položky s hodnotou 0
	 * @return array Data
	 */
	public function getData()
	{
		$empty = array_fill_keys(array_values($this->values), 0);
		return array_intersect_key($this->getSentData() + $empty, $empty);
	}

	/**
	 * Vrátí klíče položek
	 * @return array Klíče položek
	 */
	public function getKeys()
	{
		return array_values($this->values);
	}

	/**
	 * Nastaví horizontální orientaci položek
	 * @param bool $horizontal Pokud je true, jsou položky řazeny vedle sebe
	 * @return CheckList $this CheckList
	 */
	public function setHorizontal($horizontal)
	{
		$class = $horizontal ? 'radio-horizontal' : 'radio-vertical';
		$this->addClass($class);
		return $this;
	}

	/**
	 * Nastaví možné hodnoty k výběru
	 * @param array $values Hodnoty jako asociativní pole, kde klíč je popisek a hodnota odesílaná hodnota
	 * @return CheckList $this CheckList
	 */
	public function setValues($values)
	{
		$this->values = $values;
		return $this;
	}

	/**
	 * Nastaví vybrané hodnoty
	 * @param array $values Vybrané hodnoty
	 * @return CheckList $this CheckList
	 */
	public function setSelectedValues($values)
	{
		$this->selectedValues = $values;
		return $this;
	}

	/**
	 * Nastaví zakázané hodnoty
	 * @param array $values Zakázané hodnoty
	 * @return CheckList $this CheckList
	 */
	public function setDisabledValues($values)
	{
		$this->disabledValues = $values;
		return $this;
	}

	/**
	 * Nastaví data pro jednu položku z CheckListu
	 * @param string $key Klíč položky
	 * @param array $value Hodnota položky
	 * @throws FormException
	 */
	public function setData($key, $value)
	{
		if (!in_array($key, $this->values))
			throw new FormException('Key ' . $key . ' does not exist in ' . $this->name . ' control');

		if ($value)
			$this->selectedValues[] = $key;
		else
		{
			$key = array_search($key, $this->selectedValues);
			if ($key !== false)
				unset($this->selectedValues[$key]);
		}
	}

	/**
	 * Vyrenderuje HTML položek v CheckListu
	 * @param HtmlBuilder $builder Instance HtmlBuilder
	 * @param bool $isPostBack Zda byl odeslaný formulář
	 */
	private function renderOptions(HtmlBuilder $builder, $isPostBack)
	{
		foreach ($this->values as $key => $value)
		{
			$params = array();
			$params['type'] = 'checkbox';
			$params['name'] = $value;
			$params['value'] = 1;
			$params['id'] = $this->htmlParams['id'] . $value;

			if (($isPostBack) && ($this->sentDataKeyExists($value) && $this->getSentData($value)) || ((!$isPostBack) && (in_array($value, $this->selectedValues))))
				$params['checked'] = 'checked';
			if (in_array($value, $this->disabledValues))
				$params['disabled'] = 'disabled';

			$builder->startElement('span');
			$builder->addElement('input', $params);
			$builder->addValueElement('label', $key, array(
				'for' => $params['id'],
			));
			$builder->endElement();
		}
	}

	/**
	 * Vrátí HTML kód CheckListu
	 * @param bool $isPostBack Zda byl odeslaný formulář
	 * @return string HTML kód
	 */
	public function renderControl($isPostBack)
	{
		$builder = new HtmlBuilder();

		$builder->startElement('div', $this->htmlParams, true);
		$this->renderOptions($builder, $isPostBack);
		$builder->endElement();

		return $builder->render();
	}

}