<?php

namespace Utils\Forms\Controls;

use DateTime;
use Utils\Forms\FormControl;
use Utils\HtmlBuilder;
use Utils\UserException;
use Utils\Utility\DateUtils;

/*
 * Kontrolka k zadání data a času
 */

class DateTimePicker extends FormControl
{
	/**
	 * @var string Zadaná hodnota
	 */
	private $value;
	/**
	 * @var string Formát hodnoty
	 */
	private $format;

	/**
	 * Inicializuje instanci
	 * @param string $name Název kontrolky
	 * @param string $format Formát data/času
	 * @param string $label Popisek
	 * @param array $htmlParams HTML parametru
	 */
	public function __construct($name, $format, $label = '', $htmlParams = array())
	{
		$this->format = $format;
		parent::__construct($name, $label, $htmlParams);
	}

	/**
	 * Vrátí HTML kód kontrolky
	 * @param bool $isPostBack Zda byl formulář odeslán
	 * @return string HTML kód
	 */
	public function renderControl($isPostBack)
	{
		$value = ($isPostBack && $this->sentDataKeyExists($this->name)) ? $this->getSentData($this->name) : $this->value;
		$this->htmlParams['value'] = $value;
		
		$this->htmlParams['type'] = $this->getType($this->format);
		$builder = new HtmlBuilder();
		$builder->addElement('input', $this->htmlParams);
		return $builder->render();
	}

	private function getType(string $type): string
	{
		if ($type === DateUtils::DATE_FORMAT) {
			return "date";
		}
		if ($type === DateUtils::TIME_FORMAT) {
			return "time";
		}
		return "datetime-local";
	}

	/**
	 * Vrátí data v kontrolce
	 * @return array Data
	 */
	public function getData()
	{
		try {
			return $this->sentDataKeyExists($this->name) && $this->getSentData($this->name) ? array($this->name => DateUtils::parseDateTime($this->getSentData($this->name), $this->format)) : array();
		} catch (\InvalidArgumentException $ex) {
			throw new UserException($ex->getMessage());
		}
	}

	/**
	 * Nastaví zadanou hodnotu
	 * @param string $value Hodnota
	 * @return DateTimePicker $this DateTimePicker
	 */
	public function setValue($value)
	{
		$this->value = $value;
		return $this;
	}

	/**
	 * Nastaví data kontrolce
	 * @param string $key Klíč se zde nevyužívá
	 * @param string $value Zadaná hodnota
	 */
	public function setData($key, $value)
	{	
		if ($this->format == "Y-m-d HH:ii"){
			$this->value = $value;
			return;
		}
		if ($this->format == DateUtils::TIME_FORMAT)
			$date = DateTime::createFromFormat(DateUtils::DB_TIME_FORMAT, $value);
		else if ($this->format == DateUtils::DATE_FORMAT)
			$date = DateTime::createFromFormat(DateUtils::DB_DATE_FORMAT, $value);
		else
			$date = DateTime::createFromFormat(DateUtils::DB_DATETIME_FORMAT, $value);
		if ($date)
			$this->value = $date->format($this->format);
	}
}
