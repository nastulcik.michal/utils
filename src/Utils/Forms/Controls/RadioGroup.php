<?php

namespace Utils\Forms\Controls;

use Utils\Forms\FormControl;
use Utils\HtmlBuilder;
/*
 * Kontrolka s RadioButtony pro výběr jedné hodnoty ze seznamu
 */
class RadioGroup extends FormControl
{
	/**
	 * @var array Hodnoty
	 */
	public $values = array();
	/**
	 * @var string Vybrané hodnoty
	 */
	public $selectedValue = '';
	/**
	 * @var bool Určuje, zda byla vybrána hodnota, protože jen ze samotné hodnoty to nepoznáme, někdo může vybrat
	 * '' nebo 0 nebo null
	 */
	private $selectedValueSet = false;

	/**
	 * Nastaví horizontální orientaci
	 * @param bool $horizontal Zda se mají možnosti řadit vedle sebe
	 * @return RadioGroup $this RadioGroup
	 */
	public function setHorizontal($horizontal)
	{
		$class = $horizontal ? 'radio-horizontal' : 'radio-vertical';
		$this->addClass($class);
		return $this;
	}

	/**
	 * Nastaví data kontrolce
	 * @param string $key Klíč, zde se nepoužívá
	 * @param string $value Hodnota
	 */
	public function setData($key, $value)
	{
		$this->selectedValue = $value;
		$this->selectedValueSet = true;
	}

	/**
	 * Nastaví vybranou hodnotu
	 * @param string $value Vybraná hodnota
	 * @return RadioGroup $this RadioGroup
	 */
	public function setSelectedValue($value)
	{
		$this->selectedValue = $value;
		$this->selectedValueSet = true;
		return $this;
	}

	/**
	 * Nastaví hodnoty
	 * @param array $values Asociativní pole hodnot, kde klíče jsou jejich popisky
	 * @return RadioGroup $this RadioGroup
	 */
	public function setValues($values)
	{
		$this->values = $values;
		return $this;
	}

	/**
	 * Vyrenderuje možnosti
	 * @param HtmlBuilder $builder Instance HTML Builderu
	 * @param bool $isPostBack Zda byl formulář odeslán
	 */
	private function renderOptions(HtmlBuilder $builder, $isPostBack)
	{
		if ((!$this->selectedValueSet) && (count($this->values)))
		{
			$values = array_values($this->values);
			$this->selectedValue = $values[0];
			$this->selectedValueSet = true;
		}
		$i = 0;
		foreach ($this->values as $key => $value)
		{
			$i++;
			$params = array(
				'name' => $this->name,
				'id' => $this->htmlParams['id'] . $i,
				'value' => $value,
				'type' => 'radio',
			);
			if (($isPostBack && $this->sentDataKeyExists($this->name) && ($this->getSentData($this->name) == $value))
				|| (!$isPostBack && $value == $this->selectedValue))
				$params['checked'] = 'checked';
			$builder->startElement('span');
			$builder->addElement('input', $params);
			$builder->addValueElement('label', $key, array(
				'for' => $this->htmlParams['id'] . $i,
			));
			$builder->endElement();
		}
	}

	/**
	 * Vrátí HTML kód kontrolky
	 * @param bool $isPostBack Zda byl odeslaný formulář
	 * @return string HTML kód
	 */
	public function renderControl($isPostBack)
	{
		$builder = new HtmlBuilder();

		$builder->startElement('div', $this->htmlParams, true);
		$this->renderOptions($builder, $isPostBack);
		$builder->endElement();

		return $builder->render();
	}
}