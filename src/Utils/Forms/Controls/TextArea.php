<?php
namespace Utils\Forms\Controls;

use Utils\Forms\FormControl;
use Utils\HtmlBuilder;

/*
 *
 * Kontrolka pro zadávání víceřádkového textu
 */
class TextArea extends FormControl
{
	/**
	 * @var string Zadaný text
	 */
	private $text;

	/**
	 * Vrátí HTML kód kontrolky
	 * @param bool $isPostBack Zda byl odeslán formulář
	 * @return string HTML kód
	 */
	public function renderControl($isPostBack)
	{
		$value = ($isPostBack && $this->sentDataKeyExists($this->name)) ? $this->getSentData($this->name) : $this->text;
		$builder = new HtmlBuilder();
		$builder->addValueElement('textarea', $value, $this->htmlParams);
		return $builder->render();
	}

	/**
	 * Nastaví zadaný text
	 * @param string $text Text
	 * @return TextArea $this textArea
	 */
	public function setText($text)
	{
		$this->text = $text;
		return $this;
	}

	/**
	 * Nastaví data kontrolce
	 * @param string $key Klíč, zde se nevyužívá
	 * @param string $text Hodnota
	 */
	public function setData($key, $text)
	{
		$this->text = $text;
	}
}