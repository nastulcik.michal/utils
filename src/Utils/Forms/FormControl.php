<?php

namespace Utils\Forms;

use Utils\UserException;
use Utils\Utility\DateUtils;
use Utils\Utility\StringUtils;

abstract class FormControl
{
	/**
	 * Pravidlo pro povinné pole
	 */
	const RULE_REQUIRED = 0;
	/**
	 * Pravidlo pro maximální délku
	 */
	const RULE_MAX_LENGTH = 1;
	/**
	 * Pravidlo pro heslo
	 */
	const RULE_PASSWORD = 2;
	/**
	 * Pravidlo pro datum a čas
	 */
	const RULE_DATETIME = 3;
	/**
	 * Pravidlo pro regulární výraz
	 */
	const RULE_PATTERN = 4;
	/**
	 * Pravidlo pro povinný soubor
	 */
	const RULE_REQUIRED_FILE = 5;
	/**
	 * Pravidlo pro recaptchu
	 */
	const RULE_RECAPTCHA = 6;
	/**
	 * Regulární výraz pro URL
	 */
	const PATTERN_URL = '(http|https)://.*';
	/**
	 * Regulární výraz pro celá čísla
	 */
	const PATTERN_INTEGER = '[0-9]+';

	/**
	 * Regulární výraz pro float čísla
	 */
	const PATTERN_FLOAT = '[0-9\.]+';

	/**
	 * Reglární výraz pro email
	 */
	const PATTERN_EMAIL = '[a-z0-9._-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
	/**
	 * @var array Kolekce validačních pravidel
	 */
	private $rules = array();
	/**
	 * @var bool Zda je hodnota v kontrolce validní
	 */
	public $invalid;
	/**
	 * @var string Popisek u kontrolky
	 */
	public $label;
	/**
	 * @var array HTML parametry kontrolky
	 */
	public $htmlParams = array();
	/**
	 * @var string Název kontrolky. Neobsahuje [] v případě multiple hodnot.
	 */
	public $name;
	/**
	 * @var string HTTP metoda pro čtení odeslaných dat
	 */
	private $method = Form::METHOD_POST;

	/**
	 * Inicializuje instanci
	 * @param string $name Název kontrolky
	 * @param string $label Popisek
	 * @param array $htmlParams HTML parametry
	 */
	public function __construct($name, $label = '', $htmlParams = array())
	{
		$this->name = $name;
		$this->label = $label;
		$this->htmlParams = $htmlParams;
		$this->htmlParams['name'] = $name;
		$this->htmlParams['id'] = $name;
	}

	/**
	 * Nastaví tooltip, což je v HTML parametr title
	 * @param string $toolTip Tooltip
	 * @return FormControl $this FormControl
	 */
	public function setTooltip($toolTip)
	{
		$this->htmlParams['title'] = $toolTip;
		return $this;
	}

	/**
	 * Nastaví metodu, kterou byla odeslána data
	 * @param string $method Metoda odeslání dat
	 */
	public function setMethod($method)
	{
		$this->method = $method;
	}

	/**
	 * Vrátí data odeslaná nastavenou HTTP metodou
	 * @param string $key Klíč, jehož data chceme zobrazit. Pokud není zadán, vrátí se celé pole.
	 * @return mixed Odeslaná data
	 */
	public function getSentData($key = '')
	{
		if ($key)
			return ($this->method == Form::METHOD_POST) ? $_POST[$key] : $_GET[$key];
		return ($this->method == Form::METHOD_POST) ? $_POST : $_GET;
	}

	/** Zjistí, zda byl v datech formuláře odeslaný určitý klíč
	 * @param string $key Klíč
	 * @return bool Zda byl klíč odeslaný formulářem či nikoli
	 */
	public function sentDataKeyExists($key)
	{
		return ($this->method == Form::METHOD_POST) ? isset($_POST[$key]) : isset($_GET[$key]);
	}
	
	/**
	 * Vrátí data z kontrolky pro další zpracování ve formuláři
	 * @return array Data
	 */
	public function getData()
	{
		return $this->sentDataKeyExists($this->name) ? array($this->name => $this->getSentData($this->name)) : array();
	}

	/**
	 * Vrátí klíče k datům v kontrolce
	 * @return array Klíče
	 */
	public function getKeys()
	{
		return array($this->name);
	}

	/**
	 * Nastaví kontrolce data
	 * @param string $key klíč
	 * @param string $value Hodnota
	 */
	public abstract function setData($key, $value);

	/**
	 * Přidá validační pravidlo
	 * @param array $rule Pravidlo
	 * @param bool $validateClient Zda validovat na klientovi
	 * @param bool $validateServer Zda validovat na serveru
	 * @return FormControl $this Kontrolka pro další zpracování
	 */
	private function addRule($rule, $validateClient, $validateServer)
	{
		$rule['validate_client'] = $validateClient;
		$rule['validate_server'] = $validateServer;
		$this->rules[] = $rule;
		return $this;
	}

	/**
	 * Přidá do HTML parametrů klientské validace
	 */
	public function addClientParams()
	{
		foreach ($this->rules as $rule)
		{
			if ($rule['validate_client'])
			{
				switch ($rule['type'])
				{
					case self::RULE_REQUIRED:
					case self::RULE_REQUIRED_FILE:
						$this->htmlParams['required'] = 'required';
						break;
					case self::RULE_MAX_LENGTH:
						$this->htmlParams['maxlength'] = $rule['max_length'];
						break;
					case self::RULE_PATTERN:
						if (!isset($this->htmlParams['pattern']))
							$this->htmlParams['pattern'] = $rule['pattern'];
						break;
				}
			}
		}
	}

	/**
	 * Přidá pravidlo pro povinné pole
	 * @param bool $validateClient Zda validovat na klientovi
	 * @param bool $validateServer Zda validovat na serveru
	 * @return FormControl $this Kontrolka pro další zpracování
	 */
	public function addRequiredRule($validateClient = true, $validateServer = true)
	{
		return $this->addRule(array(
			'type' => self::RULE_REQUIRED,
			'message' => 'Povinné pole',
		), $validateClient, $validateServer);
	}

	/**
	 * Přidá pravidlo pro povinný soubor
	 * @param bool $validateClient Zda validovat na klientovi
	 * @param bool $validateServer Zda validovat na serveru
	 * @return FormControl $this Kontrolka pro další zpracování
	 */
	public function addFileRequiredRule($validateClient = true, $validateServer = true)
	{
		return $this->addRule(array(
			'type' => self::RULE_REQUIRED_FILE,
			'message' => 'Soubor je povinný',
		), $validateClient, $validateServer);
	}

	/**
	 * Přidá pravidlo pro maximální délku
	 * @param int $maxLength Maximální délka hodnoty
	 * @param bool $validateClient Zda validovat na klientovi
	 * @param bool $validateServer Zda validovat na serveru
	 * @return FormControl $this Kontrolka pro další zpracování
	 */
	public function addMaxLengthRule($maxLength, $validateClient = true, $validateServer = true)
	{
		return $this->addRule(array(
			'type' => self::RULE_MAX_LENGTH,
			'max_length' => $maxLength,
			'message' => 'Maximální délka hodnoty je ' . $maxLength,
		), $validateClient, $validateServer);
	}

	/**
	 * Přidá pravidlo pro minimální délku
	 * @param int $minLength Minimální délka
	 * @param bool $validateClient Zda validovat na klientovi
	 * @param bool $validateServer Zda validovat na serveru
	 * @return FormControl $this Kontrolka pro další zpracování
	 */
	public function addMinLengthRule($minLength, $validateClient = true, $validateServer = true)
	{
		return $this->addPatternRule('.{' . $minLength . ',}', $validateClient, $validateServer);
	}

	/**
	 * Přidá pravidlo pro datum a čas
	 * @param bool $validateClient Zda validovat na klientovi
	 * @param bool $validateServer Zda validovat na serveru
	 * @return FormControl $this Kontrolka pro další zpracování
	 */
	public function addDateTimeRule($validateClient = true, $validateServer = true)
	{
		// $this->addPatternRule('[0-3]?[0-9]\.[0-1]?[0-9]\.[0-9]{4}\s[0-2]?[0-9]\:[0-5]?[0-9](\:[0-5]?[0-9])?');
		// $this->addPatternRule('\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}');
		// $this->addPatternRule('(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})');
		return $this->addRule(array(
			'type' => self::RULE_DATETIME,
			'format' => DateUtils::DATETIME_FORMAT,
			'message' => 'Hodnota musí být ve formátu: dd.mm.yyyy hh:mm(:ss)',
		), $validateClient, $validateServer);
	}

	/**
	 * Přidá pravidlo pro datum
	 * @param bool $validateClient Zda validovat na klientovi
	 * @param bool $validateServer Zda validovat na serveru
	 * @return FormControl $this Kontrolka pro další zpracování
	 */
	public function addDateRule($validateClient = true, $validateServer = true)
	{
		$this->addPatternRule('[0-3]?[0-9]\.[0-1]?[0-9]\.[0-9]{4}');
		return $this->addRule(array(
			'type' => self::RULE_DATETIME,
			'format' => DateUtils::DATE_FORMAT,
			'message' => 'Hodnota musí být ve formátu: dd.mm.yyyy',
		), $validateClient, $validateServer);
	}

	/**
	 * Přidá pravidlo pro čas
	 * @param bool $validateClient Zda validovat na klientovi
	 * @param bool $validateServer Zda validovat na serveru
	 * @return FormControl $this Kontrolka pro další zpracování
	 */
	public function addTimeRule($validateClient = true, $validateServer = true)
	{
		$this->addPatternRule('([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?');
		return $this->addRule(array(
			'type' => self::RULE_DATETIME,
			'format' => DateUtils::TIME_FORMAT,
			'message' => 'Hodnota musí být ve formátu: hh:mm:ss',
		), $validateClient, $validateServer);
	}

	/**
	 * Přidá pravidlo pro heslo
	 * @param bool $validateClient Zda validovat na klientovi
	 * @param bool $validateServer Zda validovat na serveru
	 * @return FormControl $this Kontrolka pro další zpracování
	 */
	public function addPasswordRule($validateClient = true, $validateServer = true)
	{
		$this->addMinLengthRule(6, $validateClient);
		return $this->addRule(array(
			'type' => self::RULE_PASSWORD,
			'message' => 'Heslo nesmí obsahovat diakritiku a musí být dlouhé alespoň 6 znaků.',
		), $validateClient, $validateServer);
	}

	/**
	 * Přidá pravidlo pro regulární výraz
	 * @param string $pattern Regulární výraz
	 * @param bool $validateClient Zda validovat na klientovi
	 * @param bool $validateServer Zda validovat na serveru
	 * @return FormControl $this Kontrolka pro další zpracování
	 */
	public function addPatternRule($pattern, $validateClient = true, $validateServer = true)
	{
		return $this->addRule(array(
			'type' => self::RULE_PATTERN,
			'pattern' => $pattern,
			'message' => 'The value has an incorrect format',
		), $validateClient, $validateServer);
	}

	public function addRecaptchaRule($securityKey, $siteKey, $securityValue)
	{
		$validateClient = false;
		$validateServer = true;
		
		return $this->addRule([
			'type' => self::RULE_RECAPTCHA,
			'recaptcha_security_key' => $securityKey,
			'recaptcha_site_key' => $siteKey,
			'recaptcha_security_value' => $securityValue,
			'message' => 'Vypršela platnost nebo máte příliš nedůvěryhodné připojení.',
		], $validateClient, $validateServer);
	}

	/**
	 * Zvaliduje hodnotu podle pravidel
	 * @throws UserException
	 * @return boolean Zda je hodnota validní
	 */
	public function checkValidity()
	{
		foreach ($this->rules as $rule)
		{
			if (($rule['validate_server']) && (!$this->checkRule($rule)))
			{
				$this->invalid = true;
				$this->addClass('invalid');
				throw new UserException($this->label . ": " .$rule['message']);
			}
		}
	}

	/**
	 * Aplikuje na kontrolku druhé validační pravidlo
	 * @param array $rule Pravidlo
	 * @return bool Zda pravidlo platí
	 */
	private function checkRule($rule)
	{
		$name = $this->name;
		
		switch ($rule['type'])
		{
			case self::RULE_REQUIRED:
				return $this->sentDataKeyExists($name) && (is_numeric($this->getSentData($name)) || $this->getSentData($name));
			case self::RULE_MAX_LENGTH:
				return !$this->sentDataKeyExists($name) || !$this->getSentData($name) || mb_strlen($this->getSentData($name)) <= $rule['max_length'];
			case self::RULE_PATTERN:
				return !$this->sentDataKeyExists($name) || !$this->getSentData($name) || preg_match('~^' . $rule['pattern'] . '$~u', $this->getSentData($name));
			case self::RULE_REQUIRED_FILE:
				return isset($_FILES[$name]) && isset($_FILES[$name]['name']) && $_FILES[$name]['name'];
			case self::RULE_DATETIME:
				return !$this->sentDataKeyExists($name) || !$this->getSentData($name) || DateUtils::validDate($this->getSentData($name), $rule['format']);
			case self::RULE_PASSWORD:
				return !$this->sentDataKeyExists($name) || !$this->getSentData($name) || ((StringUtils::removeAccents($this->getSentData($name)) == $this->getSentData($name)) && (mb_strlen($this->getSentData($name)) >= 6));
			case self::RULE_RECAPTCHA:
				return $this->controlRecaptcha($name, $rule);
		}
		return false;
	}

	/**
	 * Control recaptcha
	 * @param  string $name Control name
	 * @param  array  $rule Rule data 		
	 * @return boolean      If all correct return true
	 */
	public function controlRecaptcha($name, $rule)
	{
		if ($this->sentDataKeyExists($name) && $this->getSentData($name)) {
			// Build POST request:
            $recaptchaUrl = 'https://www.google.com/recaptcha/api/siteverify';
            $recaptchaSecret = $rule['recaptcha_security_key'];
            $recaptchaResponse = $this->getSentData($name);

            // Make and decode POST request:
            $recaptcha = file_get_contents($recaptchaUrl . '?secret=' . $recaptchaSecret . '&response=' . $recaptchaResponse);
            $recaptcha = json_decode($recaptcha);

            // Take action based on the score returned:
            if (property_exists($recaptcha, "score")) {
            	if ($recaptcha->score >= $rule['recaptcha_security_value']) {
	            	return true;
            	}
            }
		}
        // Not verified - show form error
		return false;
	}

	/**
	 * Vrátí HTML kód kontrolky
	 * @param bool $isPostBack Zda byl odeslaný formulář
	 * @return string HTML kód
	 */
	protected abstract function renderControl($isPostBack);

	/**
	 * Vrátí HTML kód kontrolky včetně klientských validací
	 * @param bool $validateClient Zda mají být přítomné klientské validace
	 * @param bool $isPostBack Zda byl odeslán formulář
	 * @return string HTML kód kontrolky
	 */
	public function render($validateClient, $isPostBack)
	{
		if ($validateClient)
			$this->addClientParams();
		return $this->renderControl($isPostBack);
	}

	/**
	 * Přidá kontrolce CSS třídu
	 * @param string $class CSS třída
	 */
	public function addClass($class)
	{
		if (isset($this->htmlParams['class']))
			$this->htmlParams['class'] .= ' ' . $class;
		else
			$this->htmlParams['class'] = $class;
	}

}