<?php
/**
 * Namespace for Utils class
 * 
 */

namespace Utils;

/**
 * Printer
 */

/*  
// STRING importance //
0)
	Duplicate sku in table provider_pneu: 2
1)
!!!!!! Duplicate sku in table provider_pneu: 2 !!!!!
2)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!! Duplicate sku in table provider_pneu: 2 !!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

// ARRAY importance //
// TYP LIST
// [
//   "brand" => ["opel","volvo","mercedes"],
//   "fruits" => ["lemon","strawberries","cherries"],
// ];


//   brand          fruits
// ----------------------------
//    opel           lemon
//   volvo         strawberries
//  mercedes         cherries

// // TYP KEY - VALUE
// [ 
//   "name" => "henry",
//   "surname" => "Doe",
//   "age" => 24,
// ];

// ********************* Your ARRAY *************************
//             provider: gpd
//                  sku: gpd15404800000-2019
//                  ean: NULL
// pohoda_identificator: NULL
//        dodavatel_kod: 15404800000
//                nazev: Barum 155/70 R13 Brillantis 2 75T
//              vyrobce: Barum

// // MULTY ARRAY - Combination TWO SIMPLY TYPES ARRAY
// [
//   "brand" =>
//   [
//     "opel",
//     "volvo",
//     "mercedes",
//   ]
//   "person" => [ 
//     "name" => "henry",
//     "surname" => "Doe",
//     "age" => 24
//   ]
// ];
// 
class Printer
{
	// Output envirement - cli || web
	private static $output;
	private static $color;
	private static $importance;

	private static $colors = [
			// By bootstrap 
			'body' => '0;30',
			'secondary' => '1;30',
			'primary' => '0;34',
			'info' => '1;34',
			'success' => '0;32',
			'danger' => '0;31',
			'warning' => '1;33',
			'muted' => '0;37',
			'white' => '1;37',
			// Normal 
			'error' => '0;31',
		];

	private static $backgroundColors = [
		'black' => '40',
		'red' => '41',
		'green' => '42',
		'yellow' => '43',
		'blue' => '44',
		'magenta' => '45',
		'cyan' => '46',
		'light_gray' => '47',
	];

	/**
	 * Print in style command line
	 * @param  mixed   $var         Input data
	 * @param  boolean $color   	Font color by bootsrap-4 style
	 * @param  integer $importance  Importence print - default-1
	 * @return                      Print output to cli - command line
	 */
	public static function cli($var, $color = "info", $importance = 0)
	{
		self::$color = $color;
		self::$importance = $importance;

		// Call function on base what date type is $var variable 
		// Example - $var is string - call function prepareString($var); 
		// $var - data
		$output = self::{"prepare".ucfirst(gettype($var))}($var);

		self::printOutput($output);
	}

	public static function prepareString($string)
	{
		if (!self::$importance)
				return self::breakLine( self::getColoredStr($string) );

		$strlen = strlen($string);
		$slashes = self::breakLine(self::createStrByLength("-", $strlen*1.5));
		$spaces = self::createStrByLength(" ", $strlen/4);
		$textLine = $spaces . $string . $spaces;
		$textWithSlashes = $slashes . self::breakLine($textLine) . $slashes;
		return self::getColoredStr($textWithSlashes);
	}
	
	private static function prepareArray($arr, $rootIndexLength = false)
	{
		$output = "";

		// Convert array to an array of string lengths
		$lengths = array_map('strlen', array_keys($arr));

		// Longest index -> number value
		if ($rootIndexLength) {
			$header = self::createStrByLength(" ", $rootIndexLength)."↓↓↓↓↓";
			$output .= self::prepareString($header, 'white');
			$maxLength = $rootIndexLength;
		}else{
			$maxLength = max($lengths);
			$output.= self::createArrayHeader("ARRAY",$maxLength);
		}

		foreach ($arr as $key => $val) {
			$keyWithSpace = self::createStrByLength(" ", $maxLength - strlen($key)) . $key;

			if(is_array($val)){
				$output .= self::prepareString($keyWithSpace. " => Array", "white");
				$output .= self::prepareArray($val, $maxLength);
				$output = self::breakLine($output);
			}else{
				$output .= self::prepareString("$keyWithSpace => $val");
			}
		}

		return $output;
	}  

	public static function createArrayHeader($text, $maxLength)
	{
		$haeder  = "";
		$header .= self::prepareString(self::createStrByLength("/", $maxLength*2 ));
		$spaceForCenterText = self::createStrByLength(" ", ($maxLength)-strlen($text)/2 );
		$header .= self::prepareString($spaceForCenterText.$text);
		$header .= self::prepareString(self::createStrByLength("/", $maxLength*2 ));
		return $header;
	}

	private static function printOutput($output)
	{
		echo self::breakLine($output, true);
	}

	private static function breakLine($string, $begin = false)
	{
		if ($begin)
			return "\n".$string;
		return $string."\n";
	}

	// Returns colored string
	public static function getColoredStr($string) {
		$coloredString = "";

		// Check if given foreground color found
		if (isset(self::$colors[self::$color])) {
			$coloredString .= "\033[" . self::$colors[self::$color] . "m";
		}

		// Check if given background color found
		if (isset(self::$backgroundColors[self::$colors[self::$color]])) {
			$coloredString .= "\033[" . self::$backgroundColors[$backgroundColor] . "m";
		}

		// Add string and end coloring
		$coloredString .=  $string . "\033[0m";

		return $coloredString;
	}

	private static function createStrByLength($tag, $length)
	{
		// add count char in before and after text - slashes on line where is text
		$x = 0;
		$str = "";
		do {
			$str .= $tag;$x++;
		} while ($x <= $length);

		return self::breakLine($str);
	}
}