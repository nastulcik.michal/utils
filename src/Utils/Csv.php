<?php

namespace Utils;

class Csv
{
	public $csv;
	private $pointer;

	public function __construct($csvPath)
	{
		$this->csv = fopen($csvPath, "rw+");
	}

	public function convertToArr(string $delimeter = ";")
	{
		$csv_header = [];
		$csv_array = [];
		while (($row = fgetcsv($this->csv, 0, $delimeter)) !== FALSE) {
			if (empty($csv_header)) {
				$csv_header = $row;
				continue;
			}

			$index_row = [];
			foreach ($row as $key => $value) {
				$index_row[$csv_header[$key]] = $value;
				
			}
			$csv_array[] = $index_row;
		}
		return $csv_array;
	}

	public function save($pathName, $limit = 100000)
	{
		$fp = fopen($pathName, 'w');

		$count = 0;

		foreach ($this->csv as $fields) {
			fputcsv($fp, $fields);
			if ($limit <= $count) {
				break;
			}
			$count++;
		}

		fclose($fp);
	}

	/**
	 * @var array $col => column ids - names or indexes
	 * @var boolean self true => change csv || false => return only array
	 * @var boolean $columName => set if csv have column names or not
	 */
	public function onlyCol($col = [], $self = true, $columName = true)
	{
		$colCount = count($col);

		//check if isset col
		if ($colCount > 0) {

			// prepare csv
			$csv = $this->csv;

			//get first line of csv
			$firstLine = $csv[0];

			// flip arr for better check if in cell or not
			$col = array_flip($col);

			// check if exist column name  in array -> true -> set index for escape this col
			foreach ($firstLine as $k => $v) {
				if (isset($col[$v])) {
					// isset index like position column in row -> value is position column in $col -> result ;
					$colIdx[$k] = $col[$v];
				}
			}

			//without first row -> columsName
			if ($columName === false) {
				unset($csv[0]);
			}

			// if enable set new "csv" into global variable $csv
			if ($self) {
				$this->csv = $this->getColumn($csv, $col, $colIdx, $colCount);
			} else {
				// return only new csv only with set column
				return $this->getColumn($csv, $col, $colIdx, $colCount);
			}
		} else {
			echo "you must set columns - @type array";
		}
	}

	public function getColumn($csv, $col, $colIdx, $colCount)
	{
		foreach ($csv as $key => $arr) {
			foreach ($arr as $k => $v) {
				// check if exist column -> column index
				if (isset($colIdx[$k])) {
					// if variable $col has only 1 value
					if ($colCount == 1) {
						$newRow = $v;
					} else {
						// sort by position in var $col
						$newRow[$colIdx[$k]] = $v;
					}
				}
			}
			// sort newRow becouse change index -> indexing by col position name
			if ($colCount > 1) {
				ksort($newRow);
				// replace index on column name
				$newRow = array_combine(array_keys($col), $newRow);
			}

			$newCsv[$key] = $newRow;
			unset($newRow);
		}
		return $newCsv;
	}

	public function deleteCol($col = [])
	{
		//check if isset col
		if (count($col) > 0) {

			// flip arr for better check if show cell or not
			$col = array_flip($col);

			//get first line of csv
			$firstLine = $this->csv[0];

			// check if  in $col is column name -> true -> set index for escape this col
			foreach ($firstLine as $k => $v) {
				if (isset($col[$v])) {
					unset($col[$v]);
					$col[$k] = $v;
				}
			}

			foreach ($this->csv as $key => $value) {
				if (!isset($col[$key])) {
					$newRow[] = $value;
				}
				$newCsv[] = $newRow;
				unset($newRow);
			}

			$this->csv = $newCsv;

			echo "<h2>Delete columns: " . implode(',', $col) . "</h2>";
		} else {
			echo "you must set columns for deleting - @type array";
		}
	}

	public function indexByColumnVal($i)
	{
		foreach ($this->csv as $key => $v) {
			if (isset($v[$i])) {
				return false;
			}
			$newCsv[$v[$i]] = $v;
		}
		$this->csv = $newCsv;
	}

	public function viewTable($limit = 10000)
	{
		$count = 0;

		echo '<table>';
		foreach ($this->csv as $row) {
			if ($limit <= $count) {
				break;
			}
			$count++;
			echo "<tr>";
			foreach ($row as $cell) {
				echo "<td>" . $cell . "</td>";
			}
			echo "</tr>";
		}
		echo '</table>';
	}

	public function deleteRowByIds($ids)
	{
		foreach ($ids as $k => $v) {
			unset($this->csv[$v]);
		}
	}

	public function showTerminal($limit = 10, $col = false)
	{
		$count = 0;
		$columnNames = "";

		foreach ($this->csv[0] as $k => $v) {
			$columnNames .= " [" . $k . "] " . str_replace("\n", "", $v);
		}

		foreach ($this->csv as $row) {
			if ($limit <= $count) {
				break;
			}
			echo "\n//////NEXT ROW///////\n";
			$row2 = "";
			$x = 0;
			foreach ($row as $cell) {
				$row2 .= " [" . $x++ . "] " . str_replace("\n", "", $cell) . "   ";
			}
			echo "\n";
			echo $columnNames;
			echo "\n\n";
			echo $row2;
			echo "\n\n";
			$count++;
		}
	}
}
