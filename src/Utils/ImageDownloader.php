<?php 
/**
 * Namespace for Utils class
 */
namespace Utils;

use Utils\Printer;

/**
 * Update,check,download images
 */
class ImageDownloader
{
	// Wait time between download images 
	public static $wait = 1;
	
	public static function download($url){
		Printer::cli("DOWNLOAD IMG from: $url",'info');
		if ($url && $url!="") {
			$image = self::getImagesByUrl($url);
			if ($image) {
				return $image;
			}else{
				return false;
			}
		}
	}

	public static function getImagesByUrl($url,$fail=0)
	{
		if ($fail>=3){ return false; }
		
		sleep(self::$wait);

        $content = @file_get_contents($url);         
        if ($content === false) {
			Printer::cli("FAIL - download again: $url",'warning');
			if ($fail==3){return false; }
			$fail++;             
			self::getImagesByUrl($url,$fail);         
		}
		return $content;     
	} 
}
