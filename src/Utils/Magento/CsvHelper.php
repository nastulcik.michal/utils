<?php
namespace Utils\Magento;

use Utils\Magento\CsvConfigurableProduct;

/**
 * Class for easier create magento CSV for import
 */
class CsvHelper
{

	public $csv;

	public $columns;

	private $clearColumns;

	public $row;

	public $rowId = 0;

	public $configurableProduct;

	// REQUIRED COLUMNS
	// SKU
	// Attribute Set
	// Product Type
	// Product Websites
	// Weight
	// Product Online
	// Tax Class
	// Name
	// Price
	// Quantity
	
	function __construct()
	{
		$this->configurableProduct = new CsvConfigurableProduct();
	}

	public function setFirstRow($additionalAttributes=[])
	{
		$firstRow = array_merge($this->getColumnNames(), $this->formatAttrNames($additionalAttributes));
		$this->csv[] = $firstRow;

		$t = $this->getMagentoTemplate();

		// Clean array - all element empty --> ""
		$colIndex = [];
		foreach ($firstRow as $k => $v) {
			if (isset($t[$v])) {
				$colIndex[$v] = $t[$v];
			} else {
				$colIndex[$v] = "";
			}
		}
		$this->clearColumns = $colIndex;
		$this->columns = $colIndex;
	}

	public function createConfigurableProducts($configurationData)
	{
		$this->configurableProduct->setData($this->columns, $configurationData);
		$this->configurableProduct->prepareData();
		$products = $this->configurableProduct->getData();

		// Set all simple products + configurable product to csv
		foreach ($products as $key => $product) {
			$this->columns = $product;
			$this->setRow();
		}
	}

	public function enableProductNameWithAttributes()
	{
		$this->configurableProduct->enableProductNameWithAttributes();
	}

	public function setNewFirstRow($changeColumns)
	{
		$flipFirstRow = array_flip($this->csv[0]);
		foreach ($changeColumns as $current => $new) {
			$key = array_search($current,$this->csv[0]);
			// Check if key isnt null or "0"
			if ($key) {
				$this->csv[0][$key] = $new;
			}
		}
	}

	public function setCol($index,$val)
	{
		$this->columns[$index] = $val;
	}

	public function getCol($index)
	{
		return $this->columns[$index];
	}

	public function setRow()
	{
		$this->rowId++;
		$this->csv[] = $this->columns;
		$this->columns = $this->clearColumns;
	}

	public function getColumnNames()
	{
		return ["sku","store_view_code","status","qty","attribute_set_code","product_type","categories","product_websites","name","description","short_description","weight","product_online","tax_class_name","visibility","price","special_price","special_price_from_date","special_price_to_date","url_key","meta_title","meta_keywords","meta_description","base_image","base_image_label","small_image","small_image_label","thumbnail_image","thumbnail_image_label","swatch_image","swatch_image_label","created_at","updated_at","new_from_date","new_to_date","display_product_options_in","map_price","msrp_price","map_enabled","gift_message_available","custom_design","custom_design_from","custom_design_to","custom_layout_update","page_layout","product_options_container","msrp_display_actual_price_type","country_of_manufacture","related_position","crosssell_skus","crosssell_position","upsell_skus","upsell_position","additional_images","additional_image_labels","hide_from_product_page","custom_options","bundle_price_type","bundle_sku_type","bundle_price_view","bundle_weight_type","bundle_values","bundle_shipment_type","configurable_variations","configurable_variation_labels","associated_skus","delivery_time","delivery_time_term","related_skus","additional_attributes"];
	}
	public function setMagentoSubProduct()
	{
		$this->columns['visibility'] = "Not Visible Individually";
	}

	public function setMagentoConfigProduct()
	{
		$this->columns['product_type'] = "configurable";
	}

	public function getMagentoTemplate()
	{
		return [
			"sku" => "",
			"store_view_code" => "",
			"status" => 1,
			"qty" => "",
			"attribute_set_code" => "Default",
			"product_type" => "simple",
			"categories" => "",
			"product_websites" => "base",
			"name" => "",
			"description" => "",
			"short_description" => "",
			"weight" => "",
			"product_online" => "",
			"tax_class_name" => "",
			"visibility" => 'Catalog, Search', // Not Visible Individually
			"price" => "",
			"special_price" => "",
			"special_price_from_date" => "",
			"special_price_to_date" => "",
			"url_key" => "",
			"meta_title" => "",
			"meta_keywords" => "",
			"meta_description" => "",
			"base_image" => "",
			"base_image_label" => "",
			"small_image" => "",
			"small_image_label" => "",
			"thumbnail_image" => "",
			"thumbnail_image_label" => "",
			"swatch_image" => "",
			"swatch_image_label" => "",
			"created_at" => "",
			"updated_at" => "",
			"new_from_date" => "",
			"new_to_date" => "",
			"display_product_options_in" => "",
			"map_price" => "",
			"msrp_price" => "",
			"map_enabled" => "",
			"gift_message_available" => "",
			"custom_design" => "",
			"custom_design_from" => "",
			"custom_design_to" => "",
			"custom_layout_update" => "",
			"page_layout" => "",
			"product_options_container" => "",
			"msrp_display_actual_price_type" => "",
			"country_of_manufacture" => "",
			"related_skus" => "",
			"related_position" => "",
			"crosssell_skus" => "",
			"crosssell_position" => "",
			"upsell_skus" => "",
			"upsell_position" => "",
			"additional_images" => "",
			"additional_image_labels" => "",
			"hide_from_product_page" => "",
			"custom_options" => "",
			"bundle_price_type" => "",
			"bundle_sku_type" => "",
			"bundle_price_view" => "",
			"bundle_weight_type" => "",
			"bundle_values" => "",
			"bundle_shipment_type" => "",
			"configurable_variations" => "",
			"configurable_variation_labels" => "",
			"associated_skus" => "",
			"delivery_time" => "",
			"delivery_time_term" => "",
			"related_skus" => "",
			"upsell_skus" => "",
			"crosssell_skus",
			'additional_attributes',
		];
	}

	public function setImage($imgPath,$alt_image)
	{
		$this->setCol('base_image',$imgPath);
		$this->setCol('base_image_label',$alt_image);
		$this->setCol('small_image',$imgPath);
		$this->setCol('small_image_label',$alt_image);
		$this->setCol('thumbnail_image',$imgPath);
		$this->setCol('thumbnail_image_label',$alt_image);
	}

	// format attr names for magento import
	public function formatAttrNames($attributes)
	{
		if (count($attributes) < 1 ) {
			return [];
		}
		foreach ($attributes as $key => $v) {
			$format = preg_replace('/[^a-z0-9\_]/','',strtolower(str_replace([" ","-"],"_",iconv('UTF-8', 'ASCII//TRANSLIT',preg_replace('/\s+/', ' ',$v)))));
			$attr[]=$format;
		}
		return $attr;
	}

	public function convertNameToUrl($str)
	{
		$str = preg_replace('/\-+/','-',preg_replace('/[^a-z0-9\-]/','',strtolower(str_replace(["/",","," ","_"],"-",trim(iconv('UTF-8', 'ASCII//TRANSLIT',preg_replace('/\s+/', ' ',$str)))))));
		return rtrim($str, '-');
	}

	public function prepareCat($cat, $separator=",", $root="Default Category")
	{
		if (is_array($cat)) {
			$cats = $cat;
		}
		elseif (strpos($cat,$separator) !== false) {
			$cats = explode($separator, $cat);
		} else {
			$cats[] = $cat;
		}

		$cats = array_filter($cats);
		if (count($cats)<1) {
			echo "no category !!!\n";
		}

		// must reverse array for create multi create path for category
		$cats = array_reverse($cats);
		$cats[] = $root;

		$this->multiPathForCat($cats);

		$implode = implode($separator,$this->magCats);
		$this->magCats = [];
		return $implode;
	}

	/*
		For Magento internal import system must be set all new category separetaly -
		Exam - pneumatiky/osobni/zimni - set product only  to cat. 'zimni'. For set product to cat. 'osobni' must add new path to osobni -> pneumatiky/osobni.
		Separate between categories is ","
	*/
	public function multiPathForCat($cats, $path="", $magCats=[], $c=0)
	{
		//get element in array
		$elm = array_pop($cats);

		//set/add path => create new path for actual cat
		// check if first path - not add slash
		if ($c==0) {
			$path .= trim($elm);
		} else {
			$path .= "/".trim($elm);
		}

		//array with magento categories
		$magCats[] = $path;

		// check categories  - when is no other categories exit -> return all create categories
		// TODO proc nevraci hodnotu pri return ?
		if (count($cats) == 0) {
			$this->magCats = $magCats;
			return;
		}

		//up counter
		$c++;

		// call again for create next path
		$this->multiPathForCat($cats, $path, $magCats, $c);
	}

	public function deleteEmptyColumn()
	{
		//counter no empty column - set columns names like index
		$counter = $this->clearColumns;

		foreach ($this->csv as $k => $row) {
			if ($k==0) {
				// set column no empty count to 0
				foreach ($row as $index => $columnName) {
					$col[$columnName] = 0;
				}
			} else {
				foreach ($row as $index => $v) {
					if ( $v != "" || $v == "0") {
						$col[$index] = $col[$index]+1;
					}
				}
			}
		}

		// delete empty column - delete element with value 0
		$noEmptyColumn = array_filter($col);

		// prepare var for new csv(array) - set first row for set after walk array -> after i know which column havent empty
		// set new csv header - columns names
		$newCsv[0] = array_keys($noEmptyColumn);

		$newRow = [];

		foreach ($this->csv as $k => $row) {
			// Skip first row
			if ($k==0) { continue; }

			// Set only no empty column to create new row
			foreach ($row as $index => $val) {
				if (isset($noEmptyColumn[$index])) {
					$newRow[$index] = $val;
				}
			}
			// Set new row to csv - without empty column
			$newCsv[] = $newRow;

			// Defense duplicate values
			$newRow = [];
		}

		// set new csv
		$this->csv = $newCsv;
	}

	public function getLastSetRow()
	{
		return $this->csv[$this->rowId];
	}

	public function reset()
	{
		$this->csv = [];
		$this->columns = [];
	}
}
