<?php 
namespace Utils\Magento;
 
use Utils\Utility\StringUtils;

/**
 * Class for create magento configurable product 
 * Co se meni - cena,parametry,nazev,url,sku
 * SKU
 * normalni: l2131ot + parametry - vysledek: l2131ot-ne-sampan - hodnoty attributu(s_postiskem a color)
 * nazev - muze zustat stejny ale pro lepsi prehlednost - L2131ot - Jmenovky s potiskem - L2131ot - Ano|Šampaň
 * url - stejne jako sku a to - l2131ot-ne-sampan
 *
 * // Rozdily Simple product pod configurable product oproti normalnimu produktu //
 *
 * *** Simple product pod konfigurovatelnym produktem ***
 * sku - l2131ot + parametry - vysledek: l2131ot-ne-sampan - hodnoty attributu(s_postiskem a color)
 * visibility - Not Visible Individually
 * Obrazky jsou zbytecne proto nenahravat staci prvni pro admina aby se videlo k cemu to patri pri nahladu v gridu v adminu
 * Take je zbytecne nahravat Crossell product a Related product - pouze configurable product
 * Nahrani pod column(additional_attributes) vsechny attributy - napr. s_potiskem=ano,color=Šampaň - pouze pridat na konec retezce 
 * 
 * *** Configurable product rozdily oproti normalnimu produktu ***
 * configurable_variations- sku=l2131ot-ne-Šampaň,s_potiskem=ne,color=Šampaň|sku=l2131ot-ano-Šampaň,s_potiskem=ano,color=Šampaň	s_potiskem=S potiskem
 * configurable_variation_labels - s_potiskem=S potiskem,color=Color
 * sku - originalni sku tedy -> l2131ot
 * product type - configurable
 * name - L2102
 * 
 */
class CsvConfigurableProduct
{
	/**
	 * Configurable product
	 * @var array
	 */
	public $configurableProduct;
	
	/**
	 * All simply product 
	 * @var array
	 */
	public $simplyProducts = [];

	/**
	 * Variation Labels - example - "s_potiskem" => "S potiskem",
	 * @var array
	 */
	public $variationLabels = [];

	/**
	 * Default product - template for create all simply products + configurable product
	 * @var array
	 */
	private $defaultProduct;

	/**
	 * All attributes for create configurable product - color,size etc.
	 * @var array
	 */
	private $attributes;

	/**
	 * Different price by attribute value
	 * @var array
	 */
	private $attributePrices;

	/**
	 * Different price for some attribute
	 * @var boolean
	 */
	private $differentPrice = false;

	/**
	 * When is enabled add to Product name attribute value - normal: L2131ot, with attributes: L2131OT Šampaň ne
	 * @var boolean
	 */
	public $productNameWithAttr = false;

	/**
	 * Data for create all simply products under configurable product
	 * @var array
	 */
	private $simplyProductsData;

	public function setData($product, $configurationData)
	{
		$this->setDefaultProduct($product);
		$this->setConfigurableAttributes($configurationData['attributes']);
		$this->controlAttributeWithDifferentPrice();
		$this->setVariationLabels($configurationData['variationLabels']);
	}

	// Enable name with attributes value - L2131ot Šampaň ne
	public function enableProductNameWithAttributes()
	{
		$this->productNameWithAttr = true;
	}

	private function setDefaultProduct($product)
	{
		$this->defaultProduct = $product;
	}

	private function setConfigurableAttributes($attributes)
	{
		// Control if some attribute has different price then default product 
		$this->controlAttributeWithDifferentPrice();
		$this->attributes = $attributes;
	}

	private function controlAttributeWithDifferentPrice()
	{
		if (isset($this->attributes['price'])) {
			$this->differentPrice = true;
			$this->attributePrices = $this->attributes['price'];
			unset($this->attributes['price']);
		}
	}

	private function setVariationLabels($variationLabels)
	{
		$this->variationLabels = $variationLabels;
	}

	public function prepareData()
	{
		$this->createSimplyProducts();
		$this->createConfigurableProduct();
	}

	public function getData()
	{
		$products = array_merge($this->simplyProducts, [$this->configurableProduct]);

		// Reset variables
		$this->simplyProducts = [];
		$this->configurableProduct = [];
		
		return $products;
	}

	private function createSimplyProducts()
	{	
		// Get all required data for create all simply product under configurable product
		$this->simplyProductsData = $this->getDataForCreateSimplyProducts();

		foreach ($this->simplyProductsData['skus'] as $key => $sku) {
			$combination = $this->simplyProductsData['combinations'][$key];
			$additionAttr = $this->simplyProductsData['additional_attributes'][$key];

			// Set 'template' product
			$this->simplyProducts[$sku] = $this->defaultProduct;
 			
 			// sku - l2131ot + parametry - vysledek: l2131ot-ne-sampan - hodnoty attributu(s_postiskem a color)
			$this->simplyProducts[$sku]['sku'] = $sku;
			
			// Set url key
			$this->simplyProducts[$sku]['url_key'] = $sku;

			// Set name
			$this->simplyProducts[$sku]['name'] = $this->getProductName($combination);
			
			// Set price by attribute combination - If dont exist different price - set price from default product
			$this->simplyProducts[$sku]['price'] = $this->getProductPriceByAttribute($combination);

			// Set additional_attributes
			$this->simplyProducts[$sku]['additional_attributes'] .= ",".$additionAttr;

			// Product - Not Visible Individually
			$this->simplyProducts[$sku]['visibility'] = 'Not Visible Individually';
			$this->simplyProducts[$sku]['product_type'] = 'simple';

			// No images 
			$this->simplyProducts[$sku]['base_image'] = '';
			$this->simplyProducts[$sku]['base_image_label'] = '';
			$this->simplyProducts[$sku]['small_image'] = '';
			$this->simplyProducts[$sku]['small_image_label'] = '';
			$this->simplyProducts[$sku]['thumbnail_image'] = '';
			$this->simplyProducts[$sku]['thumbnail_image_label'] = '';
			$this->simplyProducts[$sku]['additional_images'] = '';
			$this->simplyProducts[$sku]['additional_image_labels'] = '';
			
			// No crossell and related products
			$this->simplyProducts[$sku]['crosssell_skus'] = '';
			$this->simplyProducts[$sku]['related_skus'] = '';
		}
	}

	private function getProductName($combination)
	{
		if (!$this->productNameWithAttr)
			return $this->defaultProduct['name'];

		return $this->defaultProduct['name'] . " " . implode(" ", $combination);
	}

	private function getProductPriceByAttribute($attributes)
	{
		// Control if exist some different prices by attribute. If not return price from default product
		if (!$this->differentPrice)
			return $this->defaultProduct['price'];

		foreach ($attributes as $code => $value) {
			if (isset($this->attributePrices[$code])) {
				return $this->attributePrices[$code][$value];
			}
		}
		return $this->defaultProduct['price'];
	}

	private function createConfigurableProduct()
	{
		$this->configurableProduct = $this->defaultProduct;
		$this->configurableProduct['sku'] = $this->configurableProduct['sku'];
		$this->configurableProduct['name'] = $this->defaultProduct['name'];
		$this->configurableProduct['product_type'] = 'configurable';
		$this->configurableProduct['configurable_variations'] = $this->getConfigurableVariations();
		$this->configurableProduct['configurable_variation_labels'] = $this->getConfigurableVariationLabels();
	}

	private function getDataForCreateSimplyProducts()
	{
		$data = [];

		// Attributes - color: red,black, size: xl,xxl - return array with all possible combinations
		$data['combinations'] = $this->getCombinations($this->attributes);

		// Create combination attribute for sku - l320-red-xl, l320-red-xxl, l320-black-xl, l320-black-xxl
		$data['skus'] = $this->getSkus($this->defaultProduct['sku'], $data['combinations']);

		// Example: s_potiskem=ano,color=Šampaň
		$data['additional_attributes'] = $this->getAdditionalAttributes($data['combinations']);	

		return $data;
	}

	// Create string like: sku=l2131ot-ne-Šampaň,s_potiskem=ne,color=Šampaň|sku=l2131ot-ano-Šampaň,s_potiskem=ano,color=Šampaň
	private function getConfigurableVariations()
	{
		$variations = [];
		
		foreach ($this->simplyProductsData['skus'] as $key => $value) {
			$variation = "sku=" . $value . "," . $this->simplyProductsData['additional_attributes'][$key];
			$variations[] = $variation;
		}

		return implode("|", $variations);
	}

	// Create string like: s_potiskem=S potiskem,color=Color
	private function getConfigurableVariationLabels()
	{
		$labels = [];
		foreach ($this->variationLabels as $key => $value) {
			$labels[] = $key."=".$value;
		}
		return implode(",", $labels);
	}


	private function getAdditionalAttributes($combinations)
	{
		$additionalAttributes = [];
		$combinationKeyValue = [];

		foreach ($combinations as $k => $combination) {
			foreach ($combination as $key => $value) {
				$combinationKeyValue[] = $key . "=" . $value;
			}

			$additionalAttributes[] = implode(",", $combinationKeyValue);

			// Reset array
			$combinationKeyValue = [];
		}
		
		return $additionalAttributes;
	}

	// Source - https://gist.github.com/cecilemuller/4688876 - I do little change
	private function getCombinations($arrays) {
		$result = array(array());
		foreach ($arrays as $property => $property_values) {
			$tmp = array();
			foreach ($result as $result_item) {
				foreach ($property_values as $property_value) {
					// Save many proccessing time - original code use array_merge
					$result_item[$property] = $property_value;
					$tmp[] = $result_item;
				}
			}
			$result = $tmp;
		}
		return $result;
	}

	private function getSkus($defaultSku, $combinations)
	{
		$skus = [];
		foreach ($combinations as $key => $combination) {
			$skus[] = $defaultSku . "-" . implode("-", $combination);
		}
		return $skus;
	}
}