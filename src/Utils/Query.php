<?php
/**
 * Namespace for Utils class
 */
namespace Utils;

use Utils\Db;

/**
 * Require Db class - connect to db
 * Prepare sql query on database
 */
class Query
{
    /**
     * create multiple insert PDO to mysql from variable $data
     * @param string $table - table name
     * @param array $data - multiple array with data
     * @param integer $max - max number array element insert to db in one request
     */
    public function insertMultipleRows($table,array $data,$max=1000)
    {
      // array_keys($data[key($data)]) - return all keys from first element in array
      $arrayKeys = array_keys($data[key($data)]);

      // string with all column names
      $dbColumns = "`".implode("`,`",$arrayKeys)."`";

      // set size/count column for create placeholders (?)
      $columnCount = sizeof($arrayKeys);

      // array with all params
      $params = [];

      $package = 0;

      foreach ($data as $key => $record) {
        if ($package>$max) {
          
          $this->insertToDb($table,$dbColumns,$columnCount,$params,$package);
          $params = []; $package = 0;
        }

        // create array  with all params for mysql insert
        foreach ($record as $key => $value) {
          $params[] = $value;
        }
        $package++;
      }
      // insert remaining data - last package
      $this->insertToDb($table,$dbColumns,$columnCount,$params,$package);
    }

    /**
     * create multiple insert PDO to mysql from variable $data
     * @param string $table - table name
     * @param string $dbColumns - columns name
     * @param integer $count - column count
     * @param array $params - params data
     * @param integer $package - size package - for create placeholders
     */
    private function insertToDb($table,$dbColumns,$count,$params,$package)
    {
      // create placeholders => questionMarks
      $questionMark =  "(".$this->placeholders('?', $count).")";
      $questionMarks =  $this->placeholders($questionMark, $package);
      Db::request('INSERT INTO `'.$table.'` ('.$dbColumns.') VALUES '.$questionMarks,$params);
    }

    /**
     * Create placeholders - use for mysql query
     * @param string $text - basic text
     * @param string $count - count placeholders will be create
     * @param string $separator - string between $text
     * @return string text with implement placeholder
     */
    private function placeholders($text, $count=0, $separator=",")
    {
        $result = array();
        if ($count > 0) {
            for($x=0; $x<$count; $x++) {
                $result[] = $text;
            }
        }

        return implode($separator, $result);
    }
}
