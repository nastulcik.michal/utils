<?php
/**
 * Namespace for Utils class
 */
namespace Utils;

use PDO;
use Utils\Utility\ArrayUtils;

/**
 * Wrapper pro snadnější práci s databází s použitím PDO a automatickým
 * zabezpečením parametrů (proměnných) v dotazech.
 */
class Db {

  /**
   * @var PDO Databázové spojení
   */
  private static $connection;

  /**
   * @var array Výchozí nastavení ovladače
   */
  private static $settings = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_EMULATE_PREPARES => false,
  );

  /**
   * Připojí se k databázi pomocí daných údajů
   * @param string $host Hostitel
   * @param string $user Uživatelské jméno
   * @param string $password Heslo
   * @param string $database Název databáze
   */
  public static function connect($host, $user, $password, $database) {
    if (!isset(self::$connection)) {
      self::$connection = @new PDO(
        "mysql:host=$host;dbname=$database",
        $user,
        $password,
        self::$settings
      );
    }
  }

  public static function beginTransaction()
  {
    self::$connection->beginTransaction();
  }

  public static function commit()
  {
    self::$connection->commit();
  }

  public static function rollBack()
  {
    self::$connection->rollBack();
  }

  /**
   * Spustí dotaz a vrátí z něj první řádek
   * @param string $query Dotaz
   * @param array $parameters Parametry
   * @return mixed Pole výsledku nebo false
   */
  public static function queryOne($query, $parameters = array()) {
    $statement = self::$connection->prepare($query);
    $statement->execute($parameters);
    return $statement->fetch();
  }

  /**
   * Spustí dotaz a vrátí z něj všechny řádky
   * @param string $query Dotaz
   * @param array $parameters Parametry
   * @return mixed Pole výsledků nebo false
   */
  public static function queryAll($query, $parameters = array()) {
    $statement = self::$connection->prepare($query);
    $statement->execute($parameters);
    return $statement->fetchAll();
  }

  /**
   * Spustí dotaz a vrátí z něj první sloupec prvního řádku
   * @param string $query Dotaz
   * @param array $parameters Parametry
   * @return mixed První hodnota výsledku dotazu nebo false
   */
  public static function querySingle($query, $parameters = array()) {
    $statement = self::queryOne($query, $parameters);
    return $statement ? $statement[0] : false;
  }

  /**
   * Spustí dotaz a vrátí počet ovlivněných řádků
   * @param string $query Dotaz
   * @param array $parameters Parametry
   * @return int Počet ovlivněných řádků
   */
  public static function query($query, $parameters = array()) {
    $statement = self::$connection->prepare($query);
    $statement->execute($parameters);
    return $statement->rowCount();
  }

  /**
   * Vloží do tabulky nový řádek jako data z asociativního pole
   * @param string $table Název tabulky
   * @param array $parameters Asociativní pole s daty
   * @return int Počet ovlivněných řádků
   */
  public static function insert($table, $parameters = array()) {
    return self::query("INSERT INTO `$table` (`".
      implode('`, `', array_keys($parameters)).
      "`) VALUES (".str_repeat('?,', sizeOf($parameters)-1)."?)",
      array_values($parameters));
  }

  /**
   * Změní řádek v tabulce tak, aby obsahoval data z asociativního pole
   * @param string $table Název tabulky
   * @param array $values Asociativní pole s daty
   * @param $condition Část SQL dotazu s podmínkou včetně WHERE
   * @param array $parameters Parametry dotazu
   * @return int Počet ovlivněných řádků
   */
  public static function update($table, $values = array(), $condition, $parameters = array()) {
    return self::query("UPDATE `$table` SET `".
      implode('` = ?, `', array_keys($values)).
      "` = ? " . $condition,
      array_merge(array_values($values), $parameters));
  }

  /**
   * Return simply array by keyColumn and ValueColumn
   * @param  string $query       Sql query
   * @param  string $keyColumn   Key index name
   * @param  string $valueColumn Value index name
   * @param  array  $parameters  Parameters
   * @return array              Simply array index by key Column
   */
  public static function queryPairs($query, $keyColumn, $valueColumn, $parameters = array())
  {
      return ArrayUtils::mapPairs(self::queryAll($query, $parameters), $keyColumn, $valueColumn);
  }

  /**
   * @return string Vrací ID posledně vloženého záznamu
   */
  public static function getLastId()
  {
    return self::$connection->lastInsertId();
  }

  /**
   * Multiple insert to table - only for cli
   * @param  string $table      Table name
   * @param  array  $parameters Parameters
   * @return boolean              
   */
  public static function insertAll($table, $parameters = array() , $maxCountInsert = 1000)
  {
    if(count($parameters) < $maxCountInsert){
      return self::insertAllToDb($table, $parameters);
    }
    $countParamaters = count($parameters);
    $countPackages = (int)($countParamaters / $maxCountInsert);
    for ($i=1; $i <= $countPackages; $i++) { 
      $beginIndex = $maxCountInsert * ($i-1);
      $endIndex = $beginIndex + $maxCountInsert;
      $package = array_slice($parameters, $beginIndex, $maxCountInsert);
        
      echo "Insert data package(" .$maxCountInsert . ") to db ( ". $beginIndex . "/" . $endIndex . " from " . $countParamaters ."): " . count($package). "\n";
      self::insertAllToDb($table, $package);
    }

    if($endIndex < $countParamaters ){
      echo "Insert rest of the data \n";
      self::insertAllToDb($table, array_slice($parameters, $endIndex));
    }

    return true;
  }


  private static function insertAllToDb($table, $parameters = array())
  {
      $params = array();
      $query = rtrim("INSERT INTO `$table` (`".
          implode('`, `', array_keys($parameters[0])).
          "`) VALUES " . str_repeat('(' . str_repeat('?,', sizeOf($parameters[0])-1)."?), ", sizeOf($parameters)), ', ');
      foreach ($parameters as $rows)
      {
          $params = array_merge($params, array_values($rows));
      }
      return self::query($query, $params);
  }
}
