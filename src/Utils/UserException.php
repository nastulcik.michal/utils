<?php

namespace Utils;

use Exception;

/**
 * Třída reprezentuje uživatelskou výjimku
 */
class UserException extends Exception
{

}