<?php 

namespace Utils;

use Utils\ArrayToXml;
use \Date;

// example 
/*
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"> 
  <url>
    <loc>http://www.example.com/foo.html</loc>
    <lastmod>2018-06-04</lastmod>
  </url>
</urlset>
 */

/**
 * Create sitemap
 */
class Sitemap
{
	private $sitemap = [];
	
	private $actualDate;

	private $domain;

	function __construct($domain)
	{
		$this->domain = "https://".$domain."/";
		$this->actualDate = \Date("Y-m-d");
	}

	private function saveNode($node)
	{
		$this->sitemap['url'][] = $node;
	}

	public function createXml()	
	{
		$arrayToXml = new ArrayToXml();
		$xml = $arrayToXml->buildXML($this->sitemap, 'urlset');

		// Fix problem with add param to root element
		$xml = str_replace("<urlset>", '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">', $xml);
		return $xml;
	}

	public function addRecord($urlPath)
	{
		$this->saveNode([
			"loc" => $this->domain . $urlPath,
			"lastmod" => $this->actualDate,
		]);
	}
}