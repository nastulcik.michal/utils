<?php 
/**
 * Namespace for Utils class
 */
namespace Utils;

use Utils\Printer;

/**
 * Utilities	
 */
class InfoUtils
{
	public $utilsList = [];

	function __construct()
	{
		$this->createUtilsList();
		$this->printUtilList();
	}

	private function createUtilsList()
	{
		$pathInfo = pathinfo(__FILE__);
		$files = glob($pathInfo['dirname']."/*");

		// Get only FileNames from path
		foreach ($files as $key => $path) {
			$this->utilsList[] = basename($path, ".php");
		}
	}

	public function printUtilList()
	{
		foreach ($this->utilsList as $key => $utilName) {
			Printer::cli($utilName,'danger');
		}
	}

	public function getMethodInfo($class)
	{
		$className = 'Utils\\'.$class;
		$class->{$className}();
		$comment_string= (new ReflectionClass($printer))->getMethod('cli')->getdoccomment();
	}


}